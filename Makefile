run:
	docker compose --compatibility up 

run-dev:
	docker compose -f ./dev.docker-compose.yml --compatibility up

build:
	docker compose --compatibility up --build

build-dev:
	docker compose -f ./dev.docker-compose.yml --compatibility up --build

reload-api:
	docker compose restart rust_api

app:
	./mentalo-app/build.sh

app-debug:
	./mentalo-app/build-debug.sh

test:
	STATIC_RESOURCES_DIR="./" CONTEXT=testing cargo test -- --test-threads=1

doc:
	cargo doc --no-deps

bash-api:
	docker exec -it rust_api bash

bash-mongo:
	docker exec -it mentalodb bash

build-front:
	npm run --prefix ./admin-frontend build

build-front-debug:
	npm run --prefix ./admin-frontend build debug

logs:
	docker compose logs -f