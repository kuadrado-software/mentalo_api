FROM kuadsoft/rust-openssl:bullseye-slim as builder

WORKDIR /usr/src/mentalo_api
COPY . .
RUN cargo install --locked --path .
 
FROM kuadsoft/debian-openssl:bullseye-slim
COPY --from=builder /usr/local/cargo/bin/mentalo_api /usr/local/bin/mentalo_api
CMD ["mentalo_api"]