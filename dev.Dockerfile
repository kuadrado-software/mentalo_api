FROM kuadsoft/rust-openssl:bullseye-slim
WORKDIR /usr/src/mentalo_api
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
COPY ./src ./src