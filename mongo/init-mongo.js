function getEnvVariable(envVar) {
    // Thanks for the tip: https://dev.to/jsheridanwells/dockerizing-a-mongo-database-4jf2

    const command = run("sh", "-c", `printenv --null ${envVar} >/mongoinit/${envVar}.txt`);
    // note: 'printenv --null' prevents adding line break to value

    if (command != 0) return Error("Failed to retrieve env variable : " + envVar);

    // .replace(/\0/g, '') removes the NULL characters
    return cat(`/mongoinit/${envVar}.txt`).replace(/\0/g, '');
}

db.createUser({
    user: getEnvVariable("MONGO_INITDB_NON_ROOT_USERNAME"),
    pwd: getEnvVariable("MONGO_INITDB_NON_ROOT_PASSWORD"),
    roles: [
        {
            role: "readWrite",
            db: getEnvVariable("MONGO_INITDB_DATABASE")
        }
    ]
});

db = new Mongo().getDB("mentalodb");

db.createCollection("games");
db.createCollection("administrators");

db.games.createIndex({ "name": "text" });

db.administrators.createIndex({ "username": 1 }, { unique: true });
db.administrators.createIndex({ "auth_token": 1 }, { unique: true });


run("sh", "-c", "rm -rf /mongoinit/*");