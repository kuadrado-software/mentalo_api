mongorestore \
    -d mentalodb \
    -h $HOST \
    --port 27017 \
    -u $MONGO_INITDB_NON_ROOT_USERNAME \
    -p $MONGO_INITDB_NON_ROOT_PASSWORD \
    --nsExclude mentalodb.administrators \
    /var/mongodumps/dump/mentalodb