cd /var/mongodumps && \
    mongodump --host $HOST \
    --port 27017 \
    --db $MONGO_INITDB_DATABASE \
    --username $MONGO_INITDB_NON_ROOT_USERNAME \
    --password $MONGO_INITDB_NON_ROOT_PASSWORD