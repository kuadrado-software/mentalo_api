db = new Mongo().getDB("mentalodb");

adminname = _getEnv("MONGO_INITDB_NON_ROOT_USERNAME");
adminpwd = _getEnv("MONGO_INITDB_NON_ROOT_PASSWORD");

db.auth(adminname, adminpwd);

games = db.getCollection("games");

games.aggregate([
    {
        $addFields: {
            "resources_index": {
                images: [],
                sounds: [],
            }
        }
    }
]);

function update_game(game) {
    const update_scenes = [];

    const resources_index = {
        images: [],
        sounds: [],
    };

    game.scenes.forEach(s => {
        const anim = s.animation;

        // create indexed resource for scene animation
        const resource = {
            name: anim.name,
            src: anim.src,
            frame_nb: NumberInt(anim.frame_nb),
        };

        // Update the animation inner data
        const update_anim_data = {
            name: anim.name,
            speed: NumberInt(anim.speed),
            play_once: anim.play_once,
        };

        s.animation = update_anim_data;

        // add the resource to index
        resources_index.images.push(resource);


        // prepare game_objects update
        const update_game_objects_data = []

        s.game_objects.forEach(go => {
            // create the nae for the image resource
            const auto_img_name = `${go.name}-${s.name}`;
            const pos = {
                x: NumberInt(go.position.x),
                y: NumberInt(go.position.y)
            }
            // update the game_object inner data
            const go_data = {
                name: go.name,
                image: auto_img_name,
                position: pos,
            };

            update_game_objects_data.push(go_data);

            // add the resource to index
            const go_img_res = {
                name: auto_img_name,
                src: go.image,
            };

            resources_index.images.push(go_img_res);
        });

        // update game_objects for this scene
        s.game_objects = update_game_objects_data;


        // update sound_track

        // add resource to index
        const snd_resource = {
            name: s.sound_track.name,
            src: s.sound_track.src,
        };

        resources_index.sounds.push(snd_resource);

        // update soundtrack data
        const update_soundtrack_data = {
            name: s.sound_track.name,
            _loop: s.sound_track._loop,
        };

        s.sound_track = update_soundtrack_data;

        // push the updated scene
        update_scenes.push(s);
    });

    const update_game = game;
    update_game.resources_index = resources_index;
    update_game.scenes = update_scenes;

    games.updateOne({ "_id": game._id }, {
        $set: {
            resources_index: resources_index,
            scenes: update_scenes,
        }
    });
}

games.find().forEach(g => update_game(g));