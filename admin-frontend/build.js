#!/usr/bin/env node
const { bundle } = require("simple-browser-js-bundler");
const path = require("path");
const dir = process.cwd();

bundle(
    `${dir}/src/index.js`,
    path.resolve(dir, "../static/admin-panel/assets/bundle.js"),
    {
        minify: !process.argv.includes("debug")
    }
);