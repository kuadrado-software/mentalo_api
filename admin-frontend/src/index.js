const renderer = require("object-to-html-renderer");
const GameList = require("./components/game-list");
const SearchGame = require("./components/search-game");

class RootComponent {
    render() {
        return {
            tag: "main",
            style_rules: {
                display: "grid",
                gridTemplateRows: "auto 40% 1fr",
                height: "100%",
            },
            contents: [
                { tag: "h1", contents: "Mentalo admin panel" },
                new GameList().render(),
                new SearchGame().render()

            ],
        };
    }
}

renderer.setRenderCycleRoot(new RootComponent()); // Set the component root in the renderer object
renderer.renderCycle();