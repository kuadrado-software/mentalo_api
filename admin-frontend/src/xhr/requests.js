async function get_game_with_decrypted_author_email(game) {
    return new Promise((resolve, reject) => {
        fetch_decrypted_string(game.metadata.author_email)
            .then(email => {
                game.metadata.author_email = email;
                resolve(game);
            })
            .catch(err => reject(err))
    })
}

async function fetch_game(game_id) {
    return new Promise((resolve, reject) => {
        fetch(`/game/${game_id}`).then(async res => {
            if (res.status >= 400 && res.status < 600) {
                const text = await res.text();
                reject(text);
            } else {
                resolve(await get_game_with_decrypted_author_email(await res.json()));
            }
        }).catch(e => reject(e))
    })
}

async function fetch_game_by_name(game_name) {
    return new Promise((resolve, reject) => {
        fetch(`/game/by-name/${game_name}`).then(async res => {
            if (res.status >= 400 && res.status < 600) {
                const text = await res.text();
                reject(text);
            } else {
                resolve(await get_game_with_decrypted_author_email(await res.json()));
            }
        }).catch(e => reject(e))
    })
}

async function fetch_moderated_games() {
    return new Promise((resolve, reject) => {
        fetch("/admin/moderated", { credentials: 'include' }).then(async res => {
            if (res.status >= 400 && res.status < 600) {
                const text = await res.text();
                reject(text);
            } else {
                const json = await res.json();
                resolve(json);
            }
        }).catch(e => reject(e))
    })
}

async function fetch_deletion_requests() {
    return new Promise((resolve, reject) => {
        fetch("/admin/requested-deletions", { credentials: 'include' }).then(async res => {
            if (res.status >= 400 && res.status < 600) {
                const text = await res.text();
                reject(text);
            } else {
                const json = await res.json();
                resolve(json);
            }
        }).catch(e => reject(e))
    })
}

async function fetch_pending_reviews() {
    return new Promise((resolve, reject) => {
        fetch("/admin/pending-reviews", { credentials: 'include' }).then(async res => {
            if (res.status >= 400 && res.status < 600) {
                const text = await res.text();
                reject(text);
            } else {
                const json = await res.json();
                resolve(json);
            }
        }).catch(e => reject(e))
    })
}

async function send_game_review(game, review_value) {
    return new Promise((resolve, reject) => {
        fetch("/admin/review-publication", {
            credentials: 'include',
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                game_id: game._id.$oid,
                review_value
            }),
        })
            .then(async res => {
                const text = await res.text();
                if (res.status >= 400 && res.status < 600) {
                    reject(text)
                } else {
                    resolve(text);
                }
            })
            .catch(err => reject(err))
    })
}

async function delete_game(game_id) {
    return new Promise((resolve, reject) => {
        fetch(`/admin/delete/${game_id}`, {
            credentials: 'include', method: "DELETE"
        })
            .then(async res => {
                const text = await res.text();
                if (res.status >= 400 && res.status < 600) {
                    reject(text)
                } else {
                    resolve(text);
                }
            })
            .catch(err => reject(err))
    });
}

async function fetch_decrypted_string(hashed_string) {
    return new Promise((resolve, reject) => {
        fetch("/admin/decrypt", {
            credentials: 'include',
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                string: hashed_string,
            }),
        })
            .then(async res => {
                const text = await res.text();
                if (res.status >= 400 && res.status < 600) {
                    reject(text)
                } else {
                    resolve(text);
                }
            })
            .catch(err => reject(err))
    });
}

module.exports = {
    fetch_pending_reviews,
    send_game_review,
    fetch_game,
    delete_game,
    fetch_moderated_games,
    fetch_game_by_name,
    fetch_decrypted_string,
    fetch_deletion_requests
}