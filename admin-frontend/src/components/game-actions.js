"use strict";

const { send_game_review, fetch_game, delete_game } = require("../xhr/requests");

class GameActions {
    constructor(params) {
        this.params = params;
    }

    handle_review(value) {
        send_game_review(this.params.game, value)
            .then(ok => this.params.send_review_callback(this.params.game, true, ok))
            .catch(err => this.params.send_review_callback(this.params.game, false, err));
    }

    handle_download() {
        fetch_game(this.params.game._id.$oid)
            .then(game => {
                const blob = new Blob([JSON.stringify(game)], { type: "application/json" });
                const download_url = URL.createObjectURL(blob);

                const a = document.createElement("a");
                a.href = download_url;
                a.download = `${game.name.replace(/\s+/, "-")}.mtl`;
                a.click();

                URL.revokeObjectURL(download_url);
                a.remove();
            })
            .catch(err => console.log(err));
    }

    handle_delete() {
        if (window.confirm("Are you sure you want to delete this game permanently?")) {
            delete_game(this.params.game._id.$oid)
                .then(res => this.params.send_review_callback(this.params.game, true, res))
                .catch(err => this.params.send_review_callback(this.params.game, true, err));
        }
    }

    render() {
        const { game } = this.params;
        const { reviewed_publication, moderated } = game.metadata;
        return {
            tag: "div",
            style_rules: {
                display: "grid",
                gap: "5px",
                gridTemplateColumns: "120px 120px",
                gridTemplateRows: "1fr 1fr"
            },
            contents: [
                { tag: "button", contents: "Download", onclick: this.handle_download.bind(this) },
                !reviewed_publication && { tag: "button", contents: "Accept", onclick: this.handle_review.bind(this, true) },
                !reviewed_publication && { tag: "button", contents: "Reject", onclick: this.handle_review.bind(this, false) },
                reviewed_publication && !moderated && { tag: "button", contents: "Moderate", onclick: this.handle_review.bind(this, false) },
                moderated && { tag: "button", contents: "Unmoderate", onclick: this.handle_review.bind(this, true) },
                { tag: "button", contents: "Delete", onclick: this.handle_delete.bind(this, false) },
            ]
        }
    }
}

module.exports = GameActions;