"use strict";

const renderer = require("object-to-html-renderer");
const { get_text_date } = require("../utils");
const { fetch_game, fetch_game_by_name } = require("../xhr/requests");
const GameActions = require("./game-actions");

class SearchGameResult {
    constructor(params) {
        this.params = params;
    }

    render() {
        const { game } = this.params;
        const image = game.publishing_overview.banner_image || game.scenes[0].animation.src;
        return {
            tag: "div",
            style_rules: {
                display: "grid",
                gridTemplateColumns: "200px 1fr 1fr",
            },
            contents: [
                {
                    tag: "img", src: image, style_rules: {
                        width: "100%",
                    }
                },
                {
                    tag: "div",
                    style_rules: {
                        display: "grid",
                        padding: "0 20px",
                    },
                    contents: [
                        { tag: "strong", contents: `Title : ${game.name}` },
                        { tag: "span", contents: `ID : ${game._id.$oid}` },
                        { tag: "span", contents: `Created on ${get_text_date(game.metadata.created_on.$date)}` },
                        { tag: "span", contents: `Last update on : ${get_text_date(game.metadata.last_update_on.$date)}` },
                        { tag: "span", contents: `Confirmed : ${game.metadata.confirmed_publication}` },
                        { tag: "span", contents: `Moderated : ${game.metadata.moderated}` },
                        { tag: "span", contents: `Reviewed : ${game.metadata.reviewed_publication}` },
                        { tag: "span", contents: `Author email : ${game.metadata.author_email}` },
                    ]
                },
                {
                    tag: "div",
                    style_rules: {
                        display: "grid",
                        padding: "0 20px",
                    },
                    contents: [
                        { tag: "strong", contents: "Publishing data" },
                        { tag: "hr", style_rules: { width: "100%", height: 0 } },
                        { tag: "span", contents: `Author name : ${game.publishing_overview.author_name}` },
                        { tag: "span", contents: `Author website : ${game.publishing_overview.author_website}` },
                        { tag: "span", contents: `Description : ${game.publishing_overview.description}` },
                        { tag: "span", contents: `Language : ${game.publishing_overview.language}` },
                    ]
                }

            ]
        }
    }
}

class SearchGame {
    constructor() {
        this.state = {
            init: false,
            search_request: "",
            search_by: "id",
            search_result: {}
        };

        this.fetch_game_by = {
            id: fetch_game,
            name: fetch_game_by_name
        };
    }

    handle_search(e) {
        e && e.preventDefault();
        const { search_by, search_request } = this.state;
        this.state.init = true;
        this.fetch_game_by[search_by](search_request)
            .then(json => {
                this.state.search_result = json;
            })
            .catch(err => {
                alert(err);
                this.state.search_result = {};
            })
            .finally(() => this.refresh());
    }

    handle_change_search_by(by) {
        this.state.search_by = by;
        this.refresh();
    }

    handle_toggle_full_game_mode(e) {
        this.state.search_mode.full_game = e.target.checked ? "full_game" : "overview";
        this.refresh();
    }

    handle_type_search_input(e) {
        this.state.search_request = e.target.value;
    }

    refresh() {
        renderer.subRender(this.render(), document.getElementById("search-game"), { mode: "replace" });
    }

    render() {
        const { init, search_result, search_by, search_request } = this.state;

        return {
            tag: "section",
            class: "admin-view-section",
            id: "search-game",
            contents: [
                {
                    tag: "div",
                    style_rules: {
                        display: "flex",
                        gap: "20px",
                        alignItems: "center",
                    },
                    contents: [
                        { tag: "h3", contents: "Search a game" },
                        {
                            tag: "div",
                            style_rules: {
                                display: "flex",
                                gap: "10px",
                                alignItems: "center",
                            },
                            contents: [
                                {
                                    tag: "div", contents: [
                                        {
                                            tag: "input", type: "radio", name: "search-mode", id: "search-mode-by-name",
                                            checked: search_by === "name",
                                            onchange: this.handle_change_search_by.bind(this, "name"),
                                        },
                                        { tag: "label", contents: "By name", htmlFor: "search-mode-by-name" },
                                    ]
                                },
                                {
                                    tag: "div", contents: [
                                        {
                                            tag: "input", type: "radio", name: "search-mode", id: "search-mode-by-id",
                                            checked: search_by === "id",
                                            onchange: this.handle_change_search_by.bind(this, "id"),
                                        },
                                        { tag: "label", contents: "By id", htmlFor: "search-mode-by-id" }
                                    ]
                                }
                            ]
                        },
                        {
                            tag: "form",
                            style_rules: {
                                display: "grid",
                                gridTemplateColumns: "300px 40px",
                                gap: "5px"
                            },
                            onsubmit: this.handle_search.bind(this),
                            contents: [
                                {
                                    tag: "input", type: "search", id: "search-game-input", value: search_request,
                                    oninput: this.handle_type_search_input.bind(this),
                                },
                                {
                                    tag: "input", type: "submit", value: "Go"
                                }
                            ]
                        }

                    ]
                },
                {
                    tag: "div", id: "game-search-result", contents: [
                        search_result.name ? {
                            tag: "div",
                            style_rules: {
                                display: "grid",
                                gridTemplateColumns: "auto 1fr",
                                margin: "20px 0",
                            },
                            contents: [
                                new SearchGameResult({ game: search_result }).render(),
                                new GameActions({
                                    game: search_result, send_review_callback: this.handle_search.bind(this, null) // TODO fix, dont do that if it's a delete action
                                }).render()
                            ]
                        } :
                            !init ? undefined :
                                { tag: "em", contents: "NO RESULT" }

                    ]
                }

            ]
        }
    }
}

module.exports = SearchGame;