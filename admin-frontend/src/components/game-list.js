"use strict";

const {
    fetch_moderated_games,
    fetch_pending_reviews,
    fetch_deletion_requests } = require("../xhr/requests");
const renderer = require("object-to-html-renderer");
const GameActions = require("./game-actions");
const { get_text_date } = require("../utils");


class GameListItem {
    constructor(params) {
        this.params = params;
    }

    render() {
        const { game } = this.params;
        return {
            tag: "div",
            style_rules: {
                display: "flex", flexDirection: "column", gap: "5px"
            },
            contents: [
                { tag: "strong", contents: game.name },
                { tag: "span", contents: `ID : ${game._id.$oid}` },
                { tag: "span", contents: `Created on : ${get_text_date(game.metadata.created_on.$date)}` },
                { tag: "span", contents: `Last update on : ${get_text_date(game.metadata.last_update_on.$date)}` }
            ]
        }
    }
}

class GameList {
    constructor() {
        this.state = {
            list_type_fetched: "",
            list_items: [],
            fetched_once: false,
        };

        this.id = "admin-game-list";

        this.fetch_list = {
            moderated_games: fetch_moderated_games,
            pending_reviews: fetch_pending_reviews,
            deletion_requests: fetch_deletion_requests,
        };
    }


    handle_game_review(game, success, response) {
        alert(response); // TMP create a notif cpt
        if (success) {
            this.state.list_items.splice(this.state.list_items.indexOf(game), 1);
            this.refresh();
        }
    }

    handle_fetch_list(list_type) {
        this.fetch_list[list_type]()
            .then(items => {
                this.state.list_type_fetched = list_type;
                this.state.fetched_once = true;
                this.state.list_items = items;
            })
            .catch(err_text => {
                this.state.list_items = [];
                alert(err_text)
            })
            .finally(() => this.refresh())
    }

    refresh() {
        renderer.subRender(this.render(), document.getElementById(this.id), { mode: "replace" });
    }

    render() {
        const { list_items, fetched_once, list_type_fetched } = this.state;
        return {
            tag: "section",
            id: this.id,
            class: "admin-view-section",
            contents: [
                {
                    tag: "div",
                    style_rules: {
                        display: "flex",
                        gap: "5px",
                    },
                    contents: [
                        {
                            tag: "button",
                            class: list_type_fetched === "pending_reviews" ? "button-nav selected" : "button-nav",
                            contents: "Pending reviews",
                            onclick: this.handle_fetch_list.bind(this, "pending_reviews")
                        },
                        {
                            tag: "button",
                            class: list_type_fetched === "moderated_games" ? "button-nav selected" : "button-nav",
                            contents: "Moderated games",
                            onclick: this.handle_fetch_list.bind(this, "moderated_games")
                        },
                        {
                            tag: "button",
                            class: list_type_fetched === "deletion_requests" ? "button-nav selected" : "button-nav",
                            contents: "Deletion requests",
                            onclick: this.handle_fetch_list.bind(this, "deletion_requests")
                        }
                    ]
                },
                fetched_once && list_items.length > 0 ? {
                    tag: "ol",
                    class: "game-list",
                    contents: list_items.map(game => {
                        return {
                            tag: "li",
                            contents: [
                                {
                                    tag: "div",
                                    style_rules: {
                                        display: "grid",
                                        gridTemplateColumns: "auto 1fr",
                                        gap: "10px"
                                    },
                                    contents: [
                                        new GameListItem({ game }).render(),
                                        new GameActions({ game, send_review_callback: this.handle_game_review.bind(this) }).render()
                                    ]
                                },
                                { tag: "hr" }
                            ]
                        }
                    })
                } : fetched_once && {
                    tag: "div", style_rules: { padding: "20px" }, contents: [{ tag: "em", contents: "EMPTY" }]
                }
            ]
        }
    }
}

module.exports = GameList;