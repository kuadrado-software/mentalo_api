use super::super::helpers::{get_game_aggregation_as_json_response, get_one_game_as_http_response};
use crate::{model::Game, AppState};
use actix_web::{
    get,
    web::{Data, Path, Query},
    HttpResponse, Responder,
};
use futures::stream::StreamExt;
use serde::Deserialize;
use wither::bson::{doc, oid::ObjectId};

const LIST_PAGE_LENGTH: i64 = 10;

#[derive(Deserialize)]
pub struct ListQstr {
    page: Option<i64>,
    sort_by: Option<String>,
}

#[derive(Deserialize)]
pub struct SearchQstr {
    query: String,
}

/// Get a list of game overviews.
/// The query string can set a page offset and a sorting order
/// ```/list?page=2&sort_by=date_desc```
#[get("/list")]
pub async fn games_list(data: Data<AppState>, qstr: Query<ListQstr>) -> impl Responder {
    let page = match qstr.page {
        Some(n) => {
            if n < 1 {
                1
            } else {
                n
            }
        }
        None => 1,
    };

    let i_start = (page - 1) * LIST_PAGE_LENGTH;

    let sort_by_split: Vec<&str> = (match &qstr.sort_by {
        Some(s) => {
            if s.contains("_") {
                s
            } else {
                "date_desc"
            }
        }
        None => "date_desc",
    })
    .split("_")
    .collect();

    let sort_by = sort_by_split[0];

    let sort_dir = match sort_by_split[1] {
        "asc" => 1,
        "desc" => -1,
        _ => 1,
    };

    let limit = doc! {
        "$limit": LIST_PAGE_LENGTH
    };
    let skip = doc! {
        "$skip": i_start
    };
    let valid = doc! {
            "$match": {
            "metadata.confirmed_publication": true,
            "metadata.reviewed_publication": true,
            "metadata.moderated": false,
        }
    };
    let fields = doc! {
        "$project": {
            "publishing_overview": 1,
            "name": 1,
            "metadata": {
                "created_on": 1,
                "version": 1,
                "last_update_on": 1,
            }
        }
    };
    let sort = doc! {
        "$sort":match sort_by {
            "date" => { doc!{ "metadata.created_on": sort_dir } },
            "name" => { doc!{ "name": sort_dir} },
            "author" => { doc!{ "publishing_overview.author_name": sort_dir } },
            _ => { doc!{ "metadata.created_on": -1 } }

        }
    };

    get_game_aggregation_as_json_response(&data.db, vec![skip, limit, valid, fields, sort]).await
}

/// Get a full game by id
#[get("/{id}")]
pub async fn game_by_id(data: Data<AppState>, id_str: Path<String>) -> impl Responder {
    let id_str = id_str.into_inner();

    let game_id: ObjectId = match ObjectId::with_string(&id_str) {
        Ok(oid) => oid,
        Err(_) => {
            return HttpResponse::BadRequest().body(
                "Couldn't create ObjectId from request. \"id\" string parameter may be malformed.",
            );
        }
    };

    get_one_game_as_http_response(
        &data.db,
        doc! {"_id": game_id },
        format!("Requested game with id {} was not found.", id_str),
    )
    .await
}

/// Get a game overview by id
#[get("/overview/{id}")]
pub async fn game_overview_by_id(
    app_state: Data<AppState>,
    id_str: Path<String>,
) -> impl Responder {
    let id_str = id_str.into_inner();

    let game_id: ObjectId = match ObjectId::with_string(&id_str) {
        Ok(oid) => oid,
        Err(_) => {
            return HttpResponse::BadRequest().body(
                "Couldn't create ObjectId from request. \"id\" string parameter may be malformed.",
            );
        }
    };

    let mut cursor = app_state
        .db
        .collection_with_type::<Game>("games")
        .aggregate(
            vec![
                doc! {
                        "$match": {
                        "_id": &game_id,
                    }
                },
                doc! {
                    "$project": {
                        "publishing_overview": 1,
                        "name": 1,
                        "metadata": {
                            "created_on": 1,
                            "version": 1,
                            "last_update_on": 1,
                        }
                    }
                },
            ],
            None,
        )
        .await
        .expect("Error performing aggregation on games collection.");

    match cursor.next().await {
        Some(item) => match item {
            Ok(doc) => HttpResponse::Ok().json(doc),
            Err(e) => HttpResponse::InternalServerError().body(format!("{:?}", e)),
        },
        None => HttpResponse::NotFound().body("Requested game was not found"),
    }
}

/// Get a full game by name
#[get("/by-name/{name}")]
pub async fn game_by_name(data: Data<AppState>, name: Path<String>) -> impl Responder {
    let name = name.into_inner();

    get_one_game_as_http_response(
        &data.db,
        doc! {"name": &name },
        format!("Requested game with name {} was not found.", &name),
    )
    .await
}

/// Searches games by name and returns a list of game overviews as results.
#[get("/search")]
pub async fn search_game(data: Data<AppState>, qstr: Query<SearchQstr>) -> impl Responder {
    let query = qstr.query.to_owned();

    if query.len() < 3 {
        return HttpResponse::Ok().json(Vec::<u8>::new());
    }

    let search = doc! {
            "$match": {
            "$text": {"$search": query},
            "metadata.confirmed_publication": true,
            "metadata.reviewed_publication": true,
            "metadata.moderated": false,
        }
    };

    let fields = doc! {
        "$project": {
            "publishing_overview": 1,
            "name": 1,
            "metadata": {
                "created_on": 1,
                "version": 1,
                "last_update_on": 1,
            }
        }
    };

    get_game_aggregation_as_json_response(&data.db, vec![search, fields]).await
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_read {
    use super::*;
    use crate::model::{game_fields::GamePublishingOverview, Game};
    use actix_web::{http::StatusCode, test, web::JsonConfig, App};
    use wither::bson::{oid::ObjectId, Bson, DateTime};

    #[derive(Debug, Deserialize)]
    pub struct GameListItemMetadataTestingModel {
        pub created_on: DateTime,
        pub last_update_on: DateTime,
        pub version: isize,
    }

    #[derive(Debug, Deserialize)]
    pub struct GameListItemTestingModel {
        pub _id: Option<ObjectId>,
        pub name: String,
        pub metadata: GameListItemMetadataTestingModel,
        pub publishing_overview: GamePublishingOverview,
    }

    async fn create_test_game(app_state: &AppState) -> Result<(ObjectId, String), String> {
        let test_game = Game::test_game(app_state);
        let game_name = test_game.name.to_owned();
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .insert_one(test_game, None)
            .await
        {
            Ok(inserted) => match inserted.inserted_id {
                Bson::ObjectId(id) => Ok((id, game_name)),
                _ => Err(String::from("Failed to parse inserted_id")),
            },
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn delete_test_game(app_state: &AppState, game_id: &ObjectId) -> Result<i64, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .delete_one(doc! {"_id": game_id}, None)
            .await
        {
            Ok(delete_result) => Ok(delete_result.deleted_count),
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    #[tokio::test]
    /// Makes a deserialization based assertion.
    /// Will panic if json response cannot be deserialize as Vec<GameListItemTestingModel>
    async fn test_game_list() {
        dotenv::dotenv().ok();
        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(games_list),
        )
        .await;

        let req = test::TestRequest::with_uri("/list").to_request();

        let _res: Vec<GameListItemTestingModel> = test::read_response_json(&mut app, req).await;
    }

    #[tokio::test]
    /// Creates a test game instance, fetches it by id and deletes it.
    /// Makes a deserialization based assertion over the fetched test ame instance,
    /// and ensures that the test instance has been correctly deleted.
    async fn test_game_by_id() {
        let app_state = AppState::for_test().await;
        let (test_game_id, _) = create_test_game(&app_state).await.unwrap();

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(game_by_id),
        )
        .await;

        let uri = format!("/{}", test_game_id);
        let req = test::TestRequest::with_uri(&uri).to_request();

        let _res: Game = test::read_response_json(&mut app, req).await;

        let delete_count = delete_test_game(&app_state, &test_game_id).await.unwrap();

        assert_eq!(delete_count, 1);
    }

    #[tokio::test]
    async fn test_game_overview_by_id() {
        let app_state = AppState::for_test().await;
        let (test_game_id, _) = create_test_game(&app_state).await.unwrap();

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(game_overview_by_id),
        )
        .await;

        let uri = format!("/overview/{}", test_game_id);
        let req = test::TestRequest::with_uri(&uri).to_request();
        let res = test::call_service(&mut app, req).await;
        assert_eq!(res.status(), StatusCode::OK);

        let delete_count = delete_test_game(&app_state, &test_game_id).await.unwrap();
        assert_eq!(delete_count, 1);
    }

    #[tokio::test]
    /// Fetches an unexisting game by name and asserts the response status is 404
    async fn test_game_by_unexisting_id_should_be_404() {
        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(game_by_id),
        )
        .await;
        let an_id = ObjectId::new().to_hex();
        let uri = format!("/{}", an_id);
        let req = test::TestRequest::with_uri(&uri).to_request();
        let res = test::call_service(&mut app, req).await;
        assert_eq!(res.status(), StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    /// Creates a test game instance, fetches it by name and deletes it.
    /// Makes a deserialization based assertion over the fetched test ame instance,
    /// and ensures that the test instance has been correctly deleted.
    async fn test_game_by_name() {
        let app_state = AppState::for_test().await;
        let (test_game_id, game_name) = create_test_game(&app_state).await.unwrap();

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(game_by_name),
        )
        .await;

        let uri = format!("/by-name/{}", game_name);
        let req = test::TestRequest::with_uri(&uri).to_request();

        let _res: Game = test::read_response_json(&mut app, req).await;

        let delete_count = delete_test_game(&app_state, &test_game_id).await.unwrap();

        assert_eq!(delete_count, 1);
    }

    #[tokio::test]
    /// Fetches an unexisting game by name and asserts the response status is 404
    async fn test_game_by_unexisting_name_should_be_404() {
        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(game_by_name),
        )
        .await;

        let uri = format!("/by-name/{}", "some-unexisting-MENTALOTESTING-game");
        let req = test::TestRequest::with_uri(&uri).to_request();
        let res = test::call_service(&mut app, req).await;
        assert_eq!(res.status(), 404);
    }

    #[tokio::test]
    /// Creates a test game instance, and asserts its includes in the results of a research containing the name of the test game.
    /// Makes a deserialization based assertion over the fetched test ame instance,
    /// and ensures that the test instance has been correctly deleted.
    async fn test_search_game() {
        let app_state = AppState::for_test().await;
        let (test_game_id, game_name) = create_test_game(&app_state).await.unwrap();

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(search_game),
        )
        .await;

        let uri = format!("/search?query={}", game_name);
        let req = test::TestRequest::with_uri(&uri).to_request();

        let res: Vec<GameListItemTestingModel> = test::read_response_json(&mut app, req).await;

        assert!(res.len() >= 1);
        assert_eq!(res[0].name, game_name);

        let delete_count = delete_test_game(&app_state, &test_game_id).await.unwrap();

        assert_eq!(delete_count, 1);
    }

    #[tokio::test]
    /// Creates a test game instance, make a search query with a different title and asserts the created test game is not in the results.
    async fn test_search_game_should_not_contain_test_game() {
        let app_state = AppState::for_test().await;
        let (test_game_id, game_name) = create_test_game(&app_state).await.unwrap();

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(search_game),
        )
        .await;

        let uri = format!("/search?query={}", "some%20game%20name");
        let req = test::TestRequest::with_uri(&uri).to_request();

        let res: Vec<GameListItemTestingModel> = test::read_response_json(&mut app, req).await;

        assert!(res.iter().find(|&g| { g.name == game_name }).is_none());

        let delete_count = delete_test_game(&app_state, &test_game_id).await.unwrap();

        assert_eq!(delete_count, 1);
    }

    #[tokio::test]
    /// Performs a game search with a query that is shortest than the accepted minimum (which is 3 chars)
    /// and asserts the results is an empty vector.
    /// Performs the creation and deletion of the test game to make sure the local database contains at least on game.
    async fn test_search_game_should_be_empty() {
        let app_state = AppState::for_test().await;
        let (test_game_id, _) = create_test_game(&app_state).await.unwrap();

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(search_game),
        )
        .await;

        let uri = format!("/search?query={}", "aa");
        let req = test::TestRequest::with_uri(&uri).to_request();

        let res: Vec<GameListItemTestingModel> = test::read_response_json(&mut app, req).await;

        assert_eq!(res.len(), 0);

        let delete_count = delete_test_game(&app_state, &test_game_id).await.unwrap();

        assert_eq!(delete_count, 1);
    }
}
