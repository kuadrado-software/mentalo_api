use super::super::email::{send_email, update_game_confirmation_email_multipart_body};
use crate::model::publishing::*;
use crate::model::{Game, PendingGameUpdateData, PendingGameUpdateMetaData};
use crate::translations::get_translated;
use crate::AppState;
use actix_web::{
    http::header::CONTENT_TYPE,
    put,
    web::{Data, Json, Path},
    HttpResponse, Responder,
};
use chrono::Utc;
use wither::{bson::doc, prelude::Model};

/// The endpoint to publish a new version for an existing game
#[put("/update/{locale}")]
pub async fn update_game(
    app_state: Data<AppState>,
    data: Json<PublishGameData>,
    locale: Path<String>,
) -> impl Responder {
    let locale = locale.as_str();

    let author_email = &data.user_email;

    let find_game = match Game::find_one(&app_state.db, doc! {"name": &data.game.name}, None).await
    {
        Ok(res) => match res {
            Some(game) => {
                let game_author_email_hash = match game.get_author_email() {
                    Ok(email) => email.to_owned(),
                    Err(e) => return HttpResponse::InternalServerError().body(e),
                };

                if app_state.encryption.decrypt(&game_author_email_hash) != *author_email {
                    return HttpResponse::Unauthorized()
                        .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                        .body(get_translated(
                            "Game is managed by a different email address",
                            locale,
                        ));
                }
                game
            }
            None => {
                return HttpResponse::NotFound()
                    .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                    .body(format!(
                        "{} : {}",
                        get_translated("Requested game was not found", locale),
                        &data.game.name
                    ))
            }
        },
        Err(e) => return HttpResponse::InternalServerError().body(format!("{:?}", e)),
    };

    let public_confirm_update_token = app_state.encryption.random_ascii_lc_string(24);
    let bson_date_now = wither::bson::DateTime(Utc::now());

    let pending_update = PendingGameUpdateData {
        scenes: data.game.scenes.clone(),
        game_ui_options: data.game.game_ui_options.clone(),
        starting_scene_index: data.game.starting_scene_index,
        publishing_overview: data.game.publishing_overview.clone(),
        metadata: PendingGameUpdateMetaData {
            confirm_update_token: app_state.encryption.encrypt(&public_confirm_update_token),
            created_on: bson_date_now,
        },
    };

    let game_id = match find_game.id() {
        Some(id) => id,
        None => return HttpResponse::InternalServerError().body("Game id is none."),
    };

    let err_doc = "Couldn't convert pending_update struct to document";
    let pending_update_doc = match wither::bson::to_bson(&pending_update) {
        Ok(bson_data) => match bson_data.as_document() {
            Some(d) => d.clone(),
            None => return HttpResponse::InternalServerError().body(err_doc),
        },
        Err(e) => return HttpResponse::InternalServerError().body(format!("{}: {:?}", err_doc, e)),
    };

    match find_game
        .update(
            &app_state.db,
            None,
            doc! {"$set": {
                "metadata.pending_update": pending_update_doc
            }},
            None,
        )
        .await
    {
        Ok(_) => {
            let res_email = send_email(
                &app_state,
                author_email,
                get_translated("Mentalo - Please confirm your game update", locale),
                update_game_confirmation_email_multipart_body(
                    &app_state,
                    &game_id.to_hex(),
                    &data.game.name,
                    &public_confirm_update_token,
                    locale,
                ),
            );

            if res_email.is_err() {
                println!("ERROR SENDING EMAIL");
            }

            HttpResponse::Ok()
                .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                .body(get_translated(
                    "Game update has been successfully received",
                    locale,
                ))
        }
        Err(e) => {
            HttpResponse::InternalServerError().body(format!("Error updating document: {:?}", e))
        }
    }
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_update {
    use super::*;
    use actix_web::{
        http::{Method, StatusCode},
        test,
        web::Bytes,
        App,
    };
    use wither::bson::{oid::ObjectId, Bson};

    async fn insert_test_game(app_state: &AppState) -> Result<(ObjectId, String), String> {
        let test_game = Game::test_game(app_state);
        let game_name = test_game.name.to_owned();
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .insert_one(test_game, None)
            .await
        {
            Ok(inserted) => match inserted.inserted_id {
                Bson::ObjectId(id) => Ok((id, game_name)),
                _ => Err(String::from("Failed to parse inserted_id")),
            },
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn delete_test_game(app_state: &AppState, game_id: &ObjectId) -> Result<i64, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .delete_one(doc! {"_id": game_id}, None)
            .await
        {
            Ok(delete_result) => Ok(delete_result.deleted_count),
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    fn get_request(app_state: &AppState, email_hash: &String, game: &Game) -> test::TestRequest {
        test::TestRequest::with_uri("/update/en")
            .method(Method::PUT)
            .header("Content-Type", "application/json")
            .header("Accept", "text/html")
            .set_payload(Bytes::from(
                serde_json::to_string(&PublishGameData {
                    user_email: app_state.encryption.decrypt(&email_hash),
                    game: game.clone(),
                })
                .unwrap(),
            ))
    }

    #[tokio::test]
    async fn test_update_game() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(update_game),
        )
        .await;

        let (game_id, game_name) = insert_test_game(&app_state).await.unwrap();

        let mut retrieve_inserted = app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": &game_name}, None)
            .await
            .unwrap()
            .expect("Inserted test game is none");

        retrieve_inserted.publishing_overview.description = String::from("Updated description");

        let author_email_hash = match retrieve_inserted.metadata {
            Some(ref md) => md.author_email.to_owned(),
            None => panic!("Game metadata is none"),
        };

        let req = get_request(&app_state, &author_email_hash, &retrieve_inserted).to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let retrieve_updated = app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": &game_name}, None)
            .await
            .unwrap()
            .expect("Updated test game is none");
        let pending_update = retrieve_updated.metadata.unwrap().pending_update;
        assert!(pending_update.is_some());
        assert_eq!(
            pending_update.unwrap().publishing_overview.description,
            "Updated description"
        );

        let delete_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(delete_count, 1);
    }

    #[tokio::test]
    async fn test_update_game_wrong_email_should_be_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(update_game),
        )
        .await;

        let (game_id, game_name) = insert_test_game(&app_state).await.unwrap();

        let mut retrieve_inserted = app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": &game_name}, None)
            .await
            .unwrap()
            .expect("Inserted test game is none");

        retrieve_inserted.publishing_overview.description = String::from("Updated description");

        let req = get_request(
            &app_state,
            &app_state
                .encryption
                .encrypt(&String::from("some-whatever@email.com")),
            &retrieve_inserted,
        )
        .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

        let delete_count = delete_test_game(&app_state, &game_id).await.unwrap();

        assert_eq!(delete_count, 1);
    }
}
