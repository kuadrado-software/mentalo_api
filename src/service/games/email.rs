use super::super::email::{
    publish_game_confirmation_email_multipart_body,
    request_game_deletion_confirmation_email_multipart_body, send_email,
    update_game_confirmation_email_multipart_body,
};
use crate::model::Game;
use crate::translations::get_translated;
use crate::AppState;
use actix_web::{
    http::header::CONTENT_TYPE,
    post,
    web::{Data, Form},
    HttpResponse, Responder,
};
use serde::{Deserialize, Serialize};
use wither::{
    bson::{doc, oid::ObjectId},
    prelude::Model,
};

enum ConfirmAction {
    UpdateGame,
    PublishGame,
    RequestDeleteGame,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ResendEmailFormData {
    pub email: String,
    pub game_id: String,
    pub locale: String,
    pub mode: String,
}

impl ConfirmAction {
    fn get_email_body(
        &self,
        app_state: &AppState,
        game_id: &String,
        game_name: &String,
        token: &String,
        locale: &str,
    ) -> [String; 2] {
        match self {
            Self::PublishGame => publish_game_confirmation_email_multipart_body(
                app_state, game_id, game_name, token, locale,
            ),
            Self::UpdateGame => update_game_confirmation_email_multipart_body(
                app_state, game_id, game_name, token, locale,
            ),
            Self::RequestDeleteGame => request_game_deletion_confirmation_email_multipart_body(
                app_state, game_id, game_name, token, locale,
            ),
        }
    }

    fn get_email_subject(&self, locale: &str) -> String {
        match self {
            Self::PublishGame => {
                get_translated("Mentalo - Please confirm your publication", locale)
            }
            Self::UpdateGame => get_translated("Mentalo - Please confirm your game update", locale),
            Self::RequestDeleteGame => get_translated(
                "Mentalo - Confirm the request for deletion of your game",
                locale,
            ),
        }
    }
}

/// This route should be used if a user didn't receive the confirmation email
///  after publish or update, and wants to trigger the sending of the email again.
/// The url part action_to_confirm can be either "update" or "publish".
/// The game_id url part must be a valid hexadecimal game id.
/// The locale is the lowercase language code set in the app preferences.
#[post("/send-confirmation-email")]
pub async fn send_confirmation_email(
    app_state: Data<AppState>,
    form_data: Form<ResendEmailFormData>,
) -> impl Responder {
    let locale = &form_data.locale;
    let confirm_action = match form_data.mode.as_str() {
        "update" => ConfirmAction::UpdateGame,
        "publish" => ConfirmAction::PublishGame,
        "request_deletion" => ConfirmAction::RequestDeleteGame,
        _ => return HttpResponse::BadRequest().body("Unkown action_to_confirm type"),
    };

    let game_id_str = form_data.game_id.to_owned();

    let game_id = match ObjectId::with_string(&game_id_str) {
        Ok(oid) => oid,
        Err(_) => {
            return HttpResponse::BadRequest()
                .body("Failed to convert game_id string to ObjectId. Parameter may be malformed")
        }
    };

    let game = match Game::find_one(&app_state.db, doc! {"_id": game_id}, None).await {
        Ok(opt) => match opt {
            Some(g) => g,
            None => {
                return HttpResponse::NotFound()
                    .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                    .body(get_translated("Requested game was not found", locale))
            }
        },
        Err(e) => {
            return HttpResponse::InternalServerError()
                .body(format!("Error fetching requested game : {:?}", e))
        }
    };

    let form_email = form_data.email.to_owned();

    let (author_email, token) = match &game.metadata {
        Some(md) => {
            let email = &md.author_email;
            let token = match confirm_action {
                ConfirmAction::UpdateGame => match &md.pending_update {
                    Some(pu) => &pu.metadata.confirm_update_token,
                    None => {
                        return HttpResponse::NotFound()
                            .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                            .body(get_translated(
                                "No pending update was found for that game",
                                locale,
                            ))
                    }
                },
                ConfirmAction::PublishGame => &md.confirm_publication_token,
                ConfirmAction::RequestDeleteGame => match &md.requested_deletion {
                    Some(rd) => &rd.confirmation_token,
                    None => {
                        return HttpResponse::NotFound()
                            .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                            .body(get_translated(
                                "No request for deletion was found for that game",
                                locale,
                            ))
                    }
                },
            };
            (
                app_state.encryption.decrypt(&email),
                app_state.encryption.decrypt(token),
            )
        }
        None => return HttpResponse::InternalServerError().body("Game metadata is None"),
    };

    if !form_email.as_str().eq(author_email.as_str()) {
        return HttpResponse::BadRequest()
            .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
            .body(get_translated(
                "Game is managed by a different email address",
                locale,
            ));
    }

    let email_body =
        confirm_action.get_email_body(&app_state, &game_id_str, &game.name, &token, locale);

    let res_email = send_email(
        &app_state,
        &author_email,
        confirm_action.get_email_subject(locale),
        email_body,
    );

    if res_email.is_err() {
        HttpResponse::InternalServerError().body("Error sending email")
    } else {
        HttpResponse::Ok().body("Email was successfully sent.")
    }
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_confirmation_email {
    use super::*;
    use actix_web::{
        http::{Method, StatusCode},
        test, App,
    };
    use wither::bson::{oid::ObjectId, Bson};

    async fn insert_test_game(app_state: &AppState, test_game: &Game) -> Result<ObjectId, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .insert_one(test_game.clone(), None)
            .await
        {
            Ok(inserted) => match inserted.inserted_id {
                Bson::ObjectId(id) => Ok(id),
                _ => Err(String::from("Failed to parse inserted_id")),
            },
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn delete_test_game(app_state: &AppState, game_id: &ObjectId) -> Result<i64, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .delete_one(doc! {"_id": game_id}, None)
            .await
        {
            Ok(delete_result) => Ok(delete_result.deleted_count),
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    fn get_request(mode: &str, game_id: &ObjectId, email: &str) -> test::TestRequest {
        test::TestRequest::with_uri("/send-confirmation-email")
            .method(Method::POST)
            .set_form(&ResendEmailFormData {
                mode: String::from(mode),
                email: String::from(email),
                game_id: game_id.to_hex(),
                locale: String::from("en"),
            })
    }

    #[tokio::test]
    async fn test_email_confirm_update() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(send_confirmation_email),
        )
        .await;

        let test_game = Game::test_game(&app_state).with_test_pending_update(&app_state);
        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let req = get_request("update", &game_id, "test@email.com").to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_email_confirm_unknown_action_should_be_bad_request() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(send_confirmation_email),
        )
        .await;

        let req = get_request("wrongaction", &ObjectId::new(), "test@email.com").to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::BAD_REQUEST);
    }

    #[tokio::test]
    async fn test_email_confirm_update_wrong_email_should_be_bad_request() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(send_confirmation_email),
        )
        .await;

        let test_game = Game::test_game(&app_state).with_test_pending_update(&app_state);
        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let req = get_request("update", &game_id, "wrong@email.com").to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::BAD_REQUEST);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_email_confirm_update_unkown_id_should_be_not_found() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(send_confirmation_email),
        )
        .await;

        let req = get_request("update", &ObjectId::new(), "test@email.com").to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    async fn test_email_confirm_update_no_pending_update_should_be_not_found() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(send_confirmation_email),
        )
        .await;

        let test_game = Game::test_game(&app_state);
        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let req = get_request("update", &game_id, "test@email.com").to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::NOT_FOUND);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_email_confirm_publish() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(send_confirmation_email),
        )
        .await;
        let test_game = Game::test_game(&app_state);
        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let req = get_request("publish", &game_id, "test@email.com").to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_email_confirm_request_delete() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(send_confirmation_email),
        )
        .await;

        let test_game = Game::test_game(&app_state).with_requested_deletion(&app_state, false);
        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let req = get_request("request_deletion", &game_id, "test@email.com").to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_email_confirm_request_delete_no_deletion_request_should_be_not_found() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(send_confirmation_email),
        )
        .await;

        let test_game = Game::test_game(&app_state);
        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let req = get_request("request_deletion", &game_id, "test@email.com").to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::NOT_FOUND);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }
}
