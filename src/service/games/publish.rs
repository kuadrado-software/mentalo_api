use super::super::email::{publish_game_confirmation_email_multipart_body, send_email};
use crate::model::publishing::*;
use crate::model::{Game, GameMetadata};
use crate::translations::get_translated;
use crate::AppState;
use actix_web::{
    post,
    web::{Data, Json, Path},
    HttpResponse, Responder,
};
use chrono::Utc;
use wither::bson::doc;

/// The endpoint for game publishing.
#[post("/publish/{locale}")]
pub async fn publish_game(
    app_state: Data<AppState>,
    data: Json<PublishGameData>,
    locale: Path<String>,
) -> impl Responder {
    let locale = locale.as_str();
    let author_email = &data.user_email;
    let mut new_game = data.game.clone();
    let public_confirm_pulication_token = app_state.encryption.random_ascii_lc_string(24);
    let bson_date_now = wither::bson::DateTime(Utc::now());

    new_game.license = Some(String::from(PUBLICATION_LICENSE_TEXT));

    new_game.metadata = Some(GameMetadata {
        author_email: app_state.encryption.encrypt(&author_email).to_owned(),
        created_on: bson_date_now,
        last_update_on: bson_date_now,
        confirmed_publication: false,
        reviewed_publication: false,
        moderated: false,
        confirm_publication_token: app_state
            .encryption
            .encrypt(&public_confirm_pulication_token),
        version: 1,
        pending_update: None,
        prefered_language: String::from(locale),
        requested_deletion: None,
    });

    match app_state
        .db
        .collection_with_type::<Game>("games")
        .insert_one(new_game, None)
        .await
    {
        Ok(inserted) => {
            let id = match inserted.inserted_id {
                wither::bson::Bson::ObjectId(id) => id.to_hex(),
                _ => {
                    return HttpResponse::InternalServerError().body("Failed to parse inserted_id")
                }
            };

            let res_email = send_email(
                &app_state,
                author_email,
                get_translated("Mentalo - Please confirm your publication", locale),
                publish_game_confirmation_email_multipart_body(
                    &app_state,
                    &id,
                    &data.game.name,
                    &public_confirm_pulication_token,
                    locale,
                ),
            );

            if res_email.is_err() {
                println!("ERROR SENDING EMAIL");
            }

            HttpResponse::Created().json(wither::bson::doc! {"inserted_id":id})
        }
        Err(e) => {
            // Constraint has been removed. See in the future is this is a real problem.
            // Verification of a duplicate name should be done here and not from database
            // because unique constraint on text fields are too complicated to handle
            // ("text" & "text different" will throw a dup key error)
            // if let Ok(res) = app_state
            //     .db
            //     .collection_with_type::<Game>("games")
            //     .find_one(doc! {"name": &data.game.name}, None)
            //     .await
            // {
            //     match res {
            //         Some(_) => {
            //             return HttpResponse::BadRequest().body(get_translated(
            //                 "A game with the same name already exists",
            //                 locale,
            //             ))
            //         }
            //         None => return HttpResponse::InternalServerError().body(format!("{:?}", e)),
            //     }
            // }
            HttpResponse::InternalServerError().body(format!("{:?}", e))
        }
    }
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_publishing {
    use super::*;
    use crate::model::{publishing::PublishGameData, Game};
    use actix_web::{
        http::{Method, StatusCode},
        test,
        web::{Bytes, JsonConfig},
        App,
    };
    use serde::Deserialize;
    use wither::prelude::*;

    #[derive(Deserialize, Debug)]
    struct TestPublishResponseData {
        inserted_id: String,
    }

    async fn delete_test_game(app_state: &AppState, game_name: &String) -> Result<i64, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .delete_one(doc! {"name": game_name}, None)
            .await
        {
            Ok(delete_result) => Ok(delete_result.deleted_count),
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    fn get_request(game: Option<&Game>) -> test::TestRequest {
        let mut req = test::TestRequest::with_uri("/publish/en")
            .method(Method::POST)
            .header("Content-Type", "application/json");

        req = req.set_payload(match game {
            None => Bytes::from(
                serde_json::to_string(&doc! {
                    "wrong": "data"
                })
                .unwrap(),
            ),
            Some(game) => Bytes::from(
                serde_json::to_string(&PublishGameData {
                    user_email: String::from("test@email.com"),
                    game: game.clone(),
                })
                .unwrap(),
            ),
        });

        req
    }

    #[tokio::test]
    /// Publishes a game from a serialized test game instance and asserts with deserialization response contains the inserted_id
    /// Asserts that the game has been inserted by fetching it by name in database and asserts ids are matching
    async fn test_publish_game() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(publish_game),
        )
        .await;

        let mut game = Game::test_game(&app_state);
        let game_name = game.name.to_owned();
        game.metadata = None;

        let req = get_request(Some(&game)).to_request();

        let res: TestPublishResponseData = test::read_response_json(&mut app, req).await;

        let inserted_id = res.inserted_id;
        let inserted_game_found = match app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": game.name}, None)
            .await
        {
            Ok(r) => match r {
                Some(g) => g,
                None => panic!("Inserted game is none"),
            },
            Err(e) => panic!("{:?}", e),
        };

        assert_eq!(
            inserted_id,
            inserted_game_found.id().unwrap().to_hex().to_owned()
        );

        let delete_count = delete_test_game(&app_state, &game_name).await.unwrap();
        assert_eq!(delete_count, 1);
    }

    #[tokio::test]
    /// Send invalid data in place of a PublishGameData structure and asserts response status is BAD_REQUEST
    async fn test_publish_game_err_if_wrong_payload() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(JsonConfig::default().limit(1 << 25u8))
                .service(publish_game),
        )
        .await;

        let req = get_request(None).to_request();

        let res = test::call_service(&mut app, req).await;
        assert_eq!(res.status(), StatusCode::BAD_REQUEST);
    }
}
