use super::super::email::{request_game_deletion_confirmation_email_multipart_body, send_email};
use crate::{model::Game, translations::get_translated, AppState};
use actix_web::{
    post,
    web::{Data, Form, Path},
    HttpResponse, Responder,
};
use serde::{Deserialize, Serialize};
use wither::bson::{doc, oid::ObjectId};

#[derive(Deserialize, Serialize, Debug)]
pub struct RequestDeletionData {
    pub email: String,
    pub game_id: String,
}

#[post("/request-delete/{locale}")]
pub async fn request_delete_game(
    app_state: Data<AppState>,
    form_data: Form<RequestDeletionData>,
    locale: Path<String>,
) -> impl Responder {
    let (email, game_id) = {
        let data = form_data.into_inner();
        (
            data.email,
            ObjectId::with_string(&data.game_id)
                .expect("Failed to convert game_id to ObjectId. String may be malformed"),
        )
    };

    let public_confirmation_token = app_state.encryption.random_ascii_lc_string(24);
    let private_confirmation_token = app_state.encryption.encrypt(&public_confirmation_token);
    match app_state
        .db
        .collection_with_type::<Game>("games")
        .find_one_and_update(
            doc! {"_id":&game_id, "metadata.author_email": app_state.encryption.encrypt(&email)},
            doc! {"$set": {
                "metadata.requested_deletion": {
                    "confirmation_token": &private_confirmation_token,
                    "confirmed": false,
                }
            }},
            None,
        )
        .await
    {
        Ok(res) => match res {
            Some(game) => {
                let res_email = send_email(
                    &app_state,
                    &email,
                    get_translated(
                        "Mentalo - Confirm the request for deletion of your game",
                        locale.as_str(),
                    ),
                    request_game_deletion_confirmation_email_multipart_body(
                        &app_state,
                        &game_id.to_hex(),
                        &game.name,
                        &public_confirmation_token,
                        locale.as_str(),
                    ),
                );

                if res_email.is_err() {
                    println!("ERROR SENDING EMAIL");
                }

                HttpResponse::Ok().body("Request for deletion has been successfully received")
            }
            None => HttpResponse::NotFound()
                .body(format!("Game with id {} was not found", &game_id.to_hex())),
        },
        Err(e) => HttpResponse::InternalServerError().body(format!("Database error {:?}", e)),
    }
}

#[cfg(test)]
mod test_request_deletion {
    use super::*;
    use actix_web::{
        http::{Method, StatusCode},
        test, App,
    };
    use wither::bson::Bson;

    async fn insert_test_game(app_state: &AppState, test_game: &Game) -> Result<ObjectId, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .insert_one(test_game.clone(), None)
            .await
        {
            Ok(inserted) => match inserted.inserted_id {
                Bson::ObjectId(id) => Ok(id),
                _ => Err(String::from("Failed to parse inserted_id")),
            },
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn delete_test_game(app_state: &AppState, game_id: &ObjectId) -> Result<i64, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .delete_one(doc! {"_id": game_id}, None)
            .await
        {
            Ok(delete_result) => Ok(delete_result.deleted_count),
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    #[tokio::test]
    async fn test_request_delete_game() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(request_delete_game),
        )
        .await;

        let test_game = Game::test_game(&app_state);
        // let game_name = test_game.name.to_owned();
        let game_meta = match test_game.metadata {
            Some(ref md) => md.clone(),
            None => panic!("Metadata is none"),
        };

        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let req = test::TestRequest::with_uri("/request-delete/en")
            .set_form(&RequestDeletionData {
                email: app_state.encryption.decrypt(&game_meta.author_email),
                game_id: game_id.to_hex(),
            })
            .method(Method::POST)
            .to_request();

        let resp = test::call_service(&mut app, req).await;
        assert_eq!(resp.status(), StatusCode::OK);

        let retrieve_requested_for_deletion = app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"_id": &game_id}, None)
            .await
            .unwrap()
            .unwrap();

        assert!(retrieve_requested_for_deletion
            .metadata
            .unwrap()
            .requested_deletion
            .is_some());

        let delete_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(delete_count, 1);
    }
}
