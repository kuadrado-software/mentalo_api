use super::super::email::{
    game_deletion_request_admin_notif_multipart_body, game_publication_admin_notif_multipart_body,
    game_update_admin_notif_multipart_body, send_email,
};
use crate::model::Game;
use crate::translations::get_translated;
use crate::AppState;
use actix_web::{get, http::header::CONTENT_TYPE, web::Data, HttpRequest, HttpResponse, Responder};
use wither::{
    bson::{doc, oid::ObjectId},
    prelude::Model,
};

/// A local helper to extract the request parameters and returns them as a tuple of
/// game_id: ObjectId, game_id_str: ObjectId.to_hex(), game_name: String
fn get_params(req: HttpRequest) -> Result<(ObjectId, String, String), String> {
    let confirmation_token = match req.match_info().get("confirmation_token") {
        Some(token) => token.to_string(),
        None => return Err(String::from("Unable to find confirmation_token url part")),
    };

    let game_id_str = match req.match_info().get("game_id") {
        Some(id_str) => id_str.to_string(),
        None => return Err(String::from("Unable to find game_id url part")),
    };

    let game_id = match ObjectId::with_string(&game_id_str) {
        Ok(oid) => oid,
        Err(_) => return Err(String::from(
            "Couldn't create ObjectId from request. \"game_id\" string parameter may be malformed.",
        )),
    };

    Ok((game_id, game_id_str, confirmation_token))
}

/// This is called when the author of a game clicks the confirmation links received after having published a game
#[get("/confirm-publication/{game_id}/{confirmation_token}")]
pub async fn confirm_game_publication(
    app_state: Data<AppState>,
    req: HttpRequest,
) -> impl Responder {
    let (game_id, game_id_str, confirmation_token) = match get_params(req) {
        Ok(params) => params,
        Err(e) => return HttpResponse::BadRequest().body(e),
    };

    match Game::find_one_and_update(
        &app_state.db,
        doc! {
            "_id": game_id, 
            "metadata.confirm_publication_token": app_state.encryption.encrypt(&confirmation_token.to_string()) 
        },
        doc! {"$set": {
                "metadata.confirmed_publication": true,
        }},
        None,
    )
    .await
    {
        Ok(updated) => match updated {
            Some(game) => {
                let game_meta = game.metadata.unwrap();
                let author_email = game_meta.author_email;
                let locale = &game_meta.prefered_language;

                let admin_notif_email_body = game_publication_admin_notif_multipart_body(
                    &game.name,
                    &game_id_str.to_string(),
                    &app_state.encryption.decrypt(&author_email),
                );

                let res_email = send_email(
                    &app_state,
                    &app_state.env.mentalo_email_receiver,
                    String::from("Mentalo - a new game has been released"),
                    admin_notif_email_body,
                );

                if res_email.is_err() {
                    println!("ERROR SENDING EMAIL");
                }

                return HttpResponse::Ok()
                .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                .body(format!(
                    "<p>{} : <b>{}</b><br>{}</p>",
                    get_translated("The publication of the following game was successfully confirmed", locale),
                    game.name,
                    get_translated("It will be accessible online as soon as it will be validated by the Mentalo moderation.", locale)
                ));
            }
            None => {
                return HttpResponse::NotFound().body(format!(
                    "Requested game with id {} and confirmation token {} was not found.",
                    game_id_str, confirmation_token
                ));
            }
        },
        Err(e) => {
            return HttpResponse::InternalServerError().body(format!(
                "Database error: Couldn't update game with id {}\n{}",
                game_id_str, e
            ))
        }
    };
}

/// This is called when the author of a game clicks the confirmation links received after having sent a new version for a game.
#[get("/confirm-update/{game_id}/{confirmation_token}")]
pub async fn confirm_game_update(app_state: Data<AppState>, req: HttpRequest) -> impl Responder {
    let (game_id, game_id_str, confirmation_token) = match get_params(req) {
        Ok(params) => params,
        Err(e) => return HttpResponse::BadRequest().body(e),
    };

    let filter = doc! {
        "_id": game_id.clone(),
        "metadata.pending_update.metadata.confirm_update_token": app_state.encryption.encrypt(&confirmation_token)
    };

    let mut locale = "en";

    match Game::find_one(&app_state.db, filter, None).await {
        Ok(result) => match result {
            Some(mut found_game) => {
                let game_name = found_game.name.to_owned();

                let author_email = match found_game.get_author_email() {
                    Ok(email) => email.to_owned(),
                    Err(_) => {
                        return HttpResponse::InternalServerError()
                            .body("Couldn't parse game author email")
                    }
                };

                if found_game.apply_pending_update().is_err() {
                    return HttpResponse::InternalServerError()
                        .body("Error updating game instance");
                }

                locale = match found_game.metadata {
                    Some(ref md) => md.prefered_language.as_str(),
                    None => locale,
                };

                match app_state
                    .db
                    .collection_with_type::<Game>("games")
                    .replace_one(doc! {"_id": game_id}, found_game.clone(), None)
                    .await
                {
                    Ok(_) => {
                        // send email to mentalo admin
                        let admin_notif_email_body = game_update_admin_notif_multipart_body(
                            &game_name,
                            &game_id_str.to_string(),
                            &app_state.encryption.decrypt(&author_email),
                        );

                        let res_email = send_email(
                            &app_state,
                            &app_state.env.mentalo_email_receiver,
                            format!("Mentalo - A new version of {} has been released", game_name),
                            admin_notif_email_body,
                        );

                        if res_email.is_err() {
                            println!("ERROR SENDING EMAIL");
                        }

                        HttpResponse::Ok()
                            .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                            .body(format!(
                                "<p>{} : <b>{}</b></p>",
                                get_translated(
                                    "The following game was successfully updated",
                                    locale
                                ),
                                game_name
                            ))
                    }
                    Err(e) => HttpResponse::InternalServerError()
                        .body(format!("Error updating game.\n{:?}", e)),
                }
            }
            None => HttpResponse::NotFound()
                .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                .body(get_translated("Requested game was not found", locale)),
        },
        Err(e) => HttpResponse::InternalServerError().body(format!(
            "Database error, couldn't fetch requested game.\n{:?}",
            e
        )),
    }
}

/// This is called when the author of a game clicks the confirmation links received after having sent request for deletion
#[get("/confirm-deletion/{game_id}/{confirmation_token}")]
pub async fn confirm_game_deletion_request(
    app_state: Data<AppState>,
    req: HttpRequest,
) -> impl Responder {
    let (game_id, game_id_str, confirmation_token) = match get_params(req) {
        Ok(params) => params,
        Err(e) => return HttpResponse::BadRequest().body(e),
    };

    let filter = doc! {
        "_id": game_id.clone(),
        "metadata.requested_deletion.confirmation_token": app_state.encryption.encrypt(&confirmation_token)
    };

    let mut locale = "en";

    match Game::find_one_and_update(
        &app_state.db,
        filter,
        doc! {"$set": {"metadata.requested_deletion.confirmed": true}},
        None,
    )
    .await
    {
        Ok(result) => match result {
            Some(updated) => {
                locale = match updated.metadata {
                    Some(ref md) => md.prefered_language.as_str(),
                    None => locale,
                };

                // send notif email to mentalo admin
                let res_email = send_email(
                    &app_state,
                    &app_state.env.mentalo_email_receiver,
                    format!(
                        "Mentalo - The deletion of the game {} has been requested",
                        &updated.name
                    ),
                    game_deletion_request_admin_notif_multipart_body(&updated.name, &game_id_str),
                );

                if res_email.is_err() {
                    println!("ERROR SENDING EMAIL");
                }

                HttpResponse::Ok()
                    .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                    .body(format!(
                        "{} : {}",
                        get_translated(
                            "The request for deletion has been sucessfully confirmed for the following game",
                            locale
                        ),
                        &updated.name
                    ))
            }
            None => HttpResponse::NotFound()
                .set_header(CONTENT_TYPE, "text/html; charset=utf-8")
                .body(get_translated("Requested game was not found", locale)),
        },
        Err(e) => HttpResponse::InternalServerError().body(format!(
            "Database error, couldn't fetch requested game.\n{:?}",
            e
        )),
    }
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_confirm_links {
    use super::*;
    use actix_web::{
        http::{Method, StatusCode},
        test, App,
    };
    use wither::bson::{oid::ObjectId, Bson};

    async fn insert_test_game(app_state: &AppState, test_game: &Game) -> Result<ObjectId, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .insert_one(test_game.clone(), None)
            .await
        {
            Ok(inserted) => match inserted.inserted_id {
                Bson::ObjectId(id) => Ok(id),
                _ => Err(String::from("Failed to parse inserted_id")),
            },
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn delete_test_game(app_state: &AppState, game_id: &ObjectId) -> Result<i64, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .delete_one(doc! {"_id": game_id}, None)
            .await
        {
            Ok(delete_result) => Ok(delete_result.deleted_count),
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    #[tokio::test]
    async fn test_confirm_game_publication() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(confirm_game_publication),
        )
        .await;

        let test_game = Game::test_game(&app_state).unconfirmed(&app_state);
        let game_name = test_game.name.to_owned();
        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let confirmation_token = match app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": &game_name}, None)
            .await
            .unwrap()
        {
            Some(g) => g.metadata.unwrap().confirm_publication_token,
            None => panic!("Inserted game is none"),
        };

        let uri = format!(
            "/confirm-publication/{}/{}",
            game_id.to_hex(),
            app_state.encryption.decrypt(&confirmation_token),
        );

        let req = test::TestRequest::with_uri(&uri)
            .method(Method::GET)
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let confirmed_game_meta = match app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": &game_name}, None)
            .await
            .unwrap()
        {
            Some(g) => g.metadata.unwrap(),
            None => panic!("Confirmed game is none"),
        };

        assert_eq!(confirmed_game_meta.confirmed_publication, true);
        assert_eq!(confirmed_game_meta.reviewed_publication, false);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_confirm_game_update() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(confirm_game_update),
        )
        .await;

        let test_game = Game::test_game(&app_state).with_test_pending_update(&app_state);
        let game_name = test_game.name.to_owned();
        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let confirmation_token = match app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": &game_name}, None)
            .await
            .unwrap()
        {
            Some(g) => {
                g.metadata
                    .unwrap()
                    .pending_update
                    .unwrap()
                    .metadata
                    .confirm_update_token
            }
            None => panic!("Updated game is none"),
        };

        let uri = format!(
            "/confirm-update/{}/{}",
            game_id.to_hex(),
            app_state.encryption.decrypt(&confirmation_token),
        );

        let req = test::TestRequest::with_uri(&uri)
            .method(Method::GET)
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let confirmed_game_overview = match app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": &game_name}, None)
            .await
            .unwrap()
        {
            Some(g) => g.publishing_overview,
            None => panic!("Confirmed game is none"),
        };

        assert_eq!(confirmed_game_overview.description, "updated description");

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_confirm_deletion_request() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .service(confirm_game_deletion_request),
        )
        .await;

        let test_game = Game::test_game(&app_state).with_requested_deletion(&app_state, false);
        let game_name = test_game.name.to_owned();
        let game_id = insert_test_game(&app_state, &test_game).await.unwrap();

        let confirmation_token = match app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": &game_name}, None)
            .await
            .unwrap()
        {
            Some(g) => {
                g.metadata
                    .unwrap()
                    .requested_deletion
                    .unwrap()
                    .confirmation_token
            }
            None => panic!("Inserted game is none"),
        };

        let uri = format!(
            "/confirm-deletion/{}/{}",
            game_id.to_hex(),
            app_state.encryption.decrypt(&confirmation_token),
        );

        let req = test::TestRequest::with_uri(&uri)
            .method(Method::GET)
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let confirmed_game_requested_deletion = match app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": &game_name}, None)
            .await
            .unwrap()
        {
            Some(g) => g.metadata.unwrap().requested_deletion.unwrap(),
            None => panic!("Confirmed game is none"),
        };

        assert_eq!(confirmed_game_requested_deletion.confirmed, true);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }
}
