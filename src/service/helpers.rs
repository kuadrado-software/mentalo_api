use crate::model::Game;
use actix_web::HttpResponse;
use futures::stream::StreamExt;
use wither::{bson::Document, mongodb::Database, prelude::Model};

/// Gets a Vec<Document> as an argument and use it to form an aggregation pipeline.
/// Returns the results of the request as a json http response.
/// The documents in the Vec<Document> argument must be vaild aggregation pipeline stages
/// Example:
/// ```
/// use wither::bson::doc;
/// // ...
/// get_game_aggregation_as_json_response(&db, vec![
///     doc! {
///         "$match": { "name": "some name", "metadata.moderated": false},
///     },
///     doc! {
///         "$project": { "name": 1, "publishing_overview": 1 },
///     },
///     doc! {
///         "$sort": { "metadata.created_on": -1}
///     }
/// ])
/// ```
pub async fn get_game_aggregation_as_json_response(
    db: &Database,
    aggr_pipeline: Vec<Document>,
) -> HttpResponse {
    let mut cursor = db
        .collection_with_type::<Game>("games")
        .aggregate(aggr_pipeline, None)
        .await
        .expect("Error performing aggregation on games collection.");

    let mut results: Vec<Document> = Vec::new();

    while let Some(result) = cursor.next().await {
        match result {
            Ok(document) => {
                results.push(document);
            }
            Err(_) => {
                return HttpResponse::InternalServerError().finish();
            }
        }
    }

    HttpResponse::Ok().json(results)
}

/// Performs a db request  with the filter and options provided as arguments,
/// and returns an OK json HttpResponse or a NOT_FOUND text response.
pub async fn get_one_game_as_http_response(
    db: &Database,
    filter: Document,
    err_not_found_msg: String,
) -> HttpResponse {
    match Game::find_one(db, filter, None).await {
        Ok(game) => {
            if game.is_none() {
                return HttpResponse::NotFound().body(err_not_found_msg);
            }

            HttpResponse::Ok().json(game)
        }
        Err(e) => HttpResponse::InternalServerError().body(format!("Database error: {:#?}", e)),
    }
}
