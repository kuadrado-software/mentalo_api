//! This modules contains the services that should only be accessible by an authenticated administrator
mod decrypt;
mod delete;
mod read;
mod review;
pub use decrypt::get_decrypted_string;
pub use delete::delete_game;
pub use read::*;
pub use review::review_game_publication;
