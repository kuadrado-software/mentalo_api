use crate::middleware::AuthenticatedAdminMiddleware;
use crate::AppState;
use actix_web::{
    post,
    web::{Data, Json},
    HttpRequest, HttpResponse, Responder,
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct RequestDecryptData {
    string: String,
}

/// Gets a string base64 hash as data argument and returns the decrypted value of the string.
#[post("/decrypt")]
pub async fn get_decrypted_string(
    app_state: Data<AppState>,
    data: Json<RequestDecryptData>,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let hashed_string = data.string.to_owned();

    HttpResponse::Ok().body(app_state.encryption.decrypt(&hashed_string))
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/
#[cfg(test)]
mod test_admin_decrypt {
    use super::*;
    use crate::{
        middleware::{get_auth_cookie, AuthenticatedAdminMiddleware},
        model::{AdminAuthCredentials, Administrator},
    };
    use actix_web::{
        http::{Method, StatusCode},
        test,
        web::Bytes,
        App,
    };
    // use serde_json;

    async fn get_authenticated_admin(app_state: &AppState) -> Administrator {
        Administrator::authenticated(
            app_state,
            AdminAuthCredentials {
                username: app_state.env.mentalo_default_admin_username.to_owned(),
                password: app_state.env.mentalo_default_admin_password.to_owned(),
            },
        )
        .await
        .unwrap()
    }

    async fn get_request(
        authenticated: bool,
        app_state: &AppState,
        string_hash: &String,
    ) -> test::TestRequest {
        let admin_user = get_authenticated_admin(&app_state).await;

        test::TestRequest::with_uri("/decrypt")
            .cookie(get_auth_cookie(
                "mentalo-admin-auth",
                match authenticated {
                    true => app_state
                        .encryption
                        .decrypt(&admin_user.auth_token.unwrap())
                        .to_owned(),
                    false => app_state.encryption.random_ascii_lc_string(256), // Wrong auth token
                },
            ))
            .header("Content-Type", "application/json")
            .header("Accept", "text/html")
            .method(Method::POST)
            .set_payload(Bytes::from(
                serde_json::to_string(&RequestDecryptData {
                    string: string_hash.to_owned(),
                })
                .unwrap(),
            ))
    }

    #[tokio::test]
    async fn test_decrypt_string() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(get_decrypted_string),
        )
        .await;

        let a_string = app_state.encryption.random_ascii_lc_string(16);
        let a_string_hash = app_state.encryption.encrypt(&a_string);

        let req = get_request(true, &app_state, &a_string_hash)
            .await
            .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::OK);

        let body = test::read_body(resp).await;
        assert_eq!(body, Bytes::from(a_string));
    }

    #[tokio::test]
    async fn test_decrypt_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(get_decrypted_string),
        )
        .await;

        let req = get_request(false, &app_state, &String::from("whatever"))
            .await
            .to_request();

        let resp = test::call_service(&mut app, req).await;

        assert_eq!(resp.status(), StatusCode::UNAUTHORIZED);
    }
}
