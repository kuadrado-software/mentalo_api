use super::super::helpers::get_game_aggregation_as_json_response;
use crate::middleware::AuthenticatedAdminMiddleware;
use crate::AppState;
use actix_web::{get, web::Data, HttpRequest, HttpResponse, Responder};
use wither::bson::doc;

/// Gets the list of the games pending for review
#[get("/pending-reviews")]
pub async fn get_pending_reviews(
    app_state: Data<AppState>,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let pending_for_review = doc! {
        "$match": {
            "metadata.confirmed_publication": true,
            "metadata.reviewed_publication": false,
            "metadata.moderated": false,
        }
    };
    let fields = doc! {
        "$project": {
            "publishing_overview": 1,
            "name": 1,
            "metadata": 1
        }
    };
    let sort = doc! {
        "$sort": {
            "metadata.created_on": -1
        }
    };

    get_game_aggregation_as_json_response(&app_state.db, vec![pending_for_review, fields, sort])
        .await
}

/// Get the list of the game having been moderated
#[get("/moderated")]
pub async fn get_moderated(
    app_state: Data<AppState>,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let moderated = doc! {
        "$match": {
            "metadata.moderated": true,
        }
    };

    let fields = doc! {
        "$project": {
            "publishing_overview": 1,
            "name": 1,
            "metadata": 1
        }
    };

    let sort = doc! {
        "$sort": {
            "metadata.created_on": -1
        }
    };

    get_game_aggregation_as_json_response(&app_state.db, vec![moderated, fields, sort]).await
}

/// Get the list of the game having been requested for deletion
#[get("/requested-deletions")]
pub async fn get_requested_deletions(
    app_state: Data<AppState>,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let requested_for_deletion = doc! {
        "$match": {
            "metadata.requested_deletion.confirmed": true,
        }
    };

    let fields = doc! {
        "$project": {
            "publishing_overview": 1,
            "name": 1,
            "metadata": 1
        }
    };

    get_game_aggregation_as_json_response(&app_state.db, vec![requested_for_deletion, fields]).await
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_admin_read {
    use super::*;
    use crate::{
        middleware::get_auth_cookie,
        model::{
            //game_fields::GamePublishingOverview,
            AdminAuthCredentials,
            Administrator,
            Game,
            GameMetadata,
        },
    };
    use actix_web::{http::StatusCode, test, App};
    use serde::Deserialize;
    use wither::bson::{oid::ObjectId, Bson};

    #[derive(Deserialize, Debug)]
    struct GameAdminListItem {
        //publishing_overview: GamePublishingOverview,
        name: String,
        metadata: GameMetadata,
    }

    async fn insert_test_game(
        app_state: &AppState,
        test_game: Game,
    ) -> Result<(ObjectId, String), String> {
        let game_name = test_game.name.to_owned();
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .insert_one(test_game, None)
            .await
        {
            Ok(inserted) => match inserted.inserted_id {
                Bson::ObjectId(id) => Ok((id, game_name)),
                _ => Err(String::from("Failed to parse inserted_id")),
            },
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn delete_test_game(app_state: &AppState, game_id: &ObjectId) -> Result<i64, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .delete_one(doc! {"_id": game_id}, None)
            .await
        {
            Ok(delete_result) => Ok(delete_result.deleted_count),
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn get_authenticated_admin(app_state: &AppState) -> Administrator {
        Administrator::authenticated(
            app_state,
            AdminAuthCredentials {
                username: app_state.env.mentalo_default_admin_username.to_owned(),
                password: app_state.env.mentalo_default_admin_password.to_owned(),
            },
        )
        .await
        .unwrap()
    }

    async fn get_request(
        app_state: &AppState,
        path: &str,
        authenticated: bool,
    ) -> test::TestRequest {
        let mut req = test::TestRequest::with_uri(path);

        if authenticated {
            let admin_user = get_authenticated_admin(&app_state).await;

            req = req.cookie(get_auth_cookie(
                "mentalo-admin-auth",
                app_state
                    .encryption
                    .decrypt(&admin_user.auth_token.unwrap())
                    .to_owned(),
            ));
        }
        req
    }

    #[tokio::test]
    async fn test_pending_reviews() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(get_pending_reviews),
        )
        .await;

        let (game_id, game_name) = insert_test_game(
            &app_state,
            Game::test_game(&app_state).unreviewed(&app_state),
        )
        .await
        .unwrap();

        let req = get_request(&app_state, "/pending-reviews", true)
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let results: Vec<GameAdminListItem> = test::read_body_json(res).await;

        let find_inserted = results.iter().find(|&g| g.name == game_name);
        assert!(find_inserted.is_some());
        assert_eq!(find_inserted.unwrap().metadata.reviewed_publication, false);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_pending_reviews_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(get_pending_reviews),
        )
        .await;

        let req = get_request(&app_state, "/pending-reviews", false)
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
    }

    #[tokio::test]
    async fn test_moderated() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(get_moderated),
        )
        .await;

        let (game_id, game_name) = insert_test_game(
            &app_state,
            Game::test_game(&app_state).moderated(&app_state),
        )
        .await
        .unwrap();

        let req = get_request(&app_state, "/moderated", true)
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let results: Vec<GameAdminListItem> = test::read_body_json(res).await;

        let find_inserted = results.iter().find(|&g| g.name == game_name);
        assert!(find_inserted.is_some());
        assert_eq!(find_inserted.unwrap().metadata.moderated, true);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_moderated_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(get_moderated),
        )
        .await;

        let req = get_request(&app_state, "/moderated", false)
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
    }

    #[tokio::test]
    async fn test_requested_deletions() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(get_requested_deletions),
        )
        .await;

        let (game_id, game_name) = insert_test_game(
            &app_state,
            Game::test_game(&app_state).with_requested_deletion(&app_state, true),
        )
        .await
        .unwrap();

        let req = get_request(&app_state, "/requested-deletions", true)
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let results: Vec<GameAdminListItem> = test::read_body_json(res).await;

        let find_inserted = results.iter().find(|&g| g.name == game_name);
        assert!(find_inserted.is_some());
        assert_eq!(
            find_inserted
                .unwrap()
                .metadata
                .requested_deletion
                .as_ref()
                .unwrap()
                .confirmed,
            true
        );

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_requested_deletions_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(get_requested_deletions),
        )
        .await;

        let req = get_request(&app_state, "/requested-deletions", false)
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
    }
}
