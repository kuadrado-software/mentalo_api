use super::super::email::{game_review_author_notification_multipart_body, send_email};
use crate::middleware::AuthenticatedAdminMiddleware;
use crate::model::publishing::*;
use crate::model::Game;
use crate::translations::get_translated;
use crate::AppState;
use actix_web::{
    put,
    web::{Data, Json},
    HttpRequest, HttpResponse, Responder,
};
use wither::{
    bson::{doc, oid::ObjectId},
    prelude::Model,
};

/// The endpoint to send the revie of a game by the moderation
#[put("/review-publication")]
pub async fn review_game_publication(
    app_state: Data<AppState>,
    data: Json<ReviewGameData>,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    req: HttpRequest,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let game_id_str = &data.game_id;
    let review_value = data.review_value;

    let game_id = match ObjectId::with_string(&game_id_str) {
        Ok(oid) => oid,
        Err(e) => {
            return HttpResponse::BadRequest().body(format!(
                "Couldn't create ObjectId from request id {}. \"game_id\" string parameter may be malformed.\n{:?}",
                &game_id_str, e
            ));
        }
    };

    match Game::find_one_and_update(
        &app_state.db,
        doc! {"_id": game_id },
        doc! {"$set": {
            "metadata.reviewed_publication": true,
            "metadata.moderated": !review_value,
        }},
        None,
    )
    .await
    {
        Ok(updated) => match updated {
            Some(game) => {
                let game_meta = match game.metadata {
                    Some(md) => md,
                    None => return HttpResponse::InternalServerError().finish(),
                };

                let notif_subject = match review_value {
                    true => get_translated(
                        "Mentalo - Your game is online!",
                        game_meta.prefered_language.as_str(),
                    ),
                    false => get_translated(
                        "Mentalo- Your game publication has been rejected by moderation",
                        game_meta.prefered_language.as_str(),
                    ),
                };

                let res_email = send_email(
                    &app_state,
                    &app_state.encryption.decrypt(&game_meta.author_email),
                    notif_subject,
                    game_review_author_notification_multipart_body(
                        &game.name,
                        review_value,
                        game_meta.prefered_language.as_str(),
                    ),
                );

                if res_email.is_err() {
                    println!("ERROR SENDING EMAIL");
                }

                HttpResponse::Ok().body(format!(
                    "The publication of the game {} was successfully reviewed.",
                    game.name
                ))
            }
            None => HttpResponse::NotFound().body(format!(
                "Requested game with id {}  was not found.",
                game_id_str
            )),
        },
        Err(e) => {
            println!("{:?}", e);
            return HttpResponse::InternalServerError()
                .body(format!("Couldn't update game with id {}", game_id_str,));
        }
    }
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_review {
    use super::*;
    use crate::{
        middleware::get_auth_cookie,
        model::{AdminAuthCredentials, Administrator},
    };
    use actix_web::{
        http::{Method, StatusCode},
        test,
        web::Bytes,
        App,
    };
    use wither::bson::Bson;

    async fn insert_test_game(
        app_state: &AppState,
        test_game: Game,
    ) -> Result<(ObjectId, String), String> {
        let game_name = test_game.name.to_owned();
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .insert_one(test_game, None)
            .await
        {
            Ok(inserted) => match inserted.inserted_id {
                Bson::ObjectId(id) => Ok((id, game_name)),
                _ => Err(String::from("Failed to parse inserted_id")),
            },
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn delete_test_game(app_state: &AppState, game_id: &ObjectId) -> Result<i64, String> {
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .delete_one(doc! {"_id": game_id}, None)
            .await
        {
            Ok(delete_result) => Ok(delete_result.deleted_count),
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn get_authenticated_admin(app_state: &AppState) -> Administrator {
        Administrator::authenticated(
            app_state,
            AdminAuthCredentials {
                username: app_state.env.mentalo_default_admin_username.to_owned(),
                password: app_state.env.mentalo_default_admin_password.to_owned(),
            },
        )
        .await
        .unwrap()
    }

    async fn get_request(
        review_value: bool,
        app_state: &AppState,
        game_id: &ObjectId,
        authenticated: bool,
    ) -> test::TestRequest {
        let admin_user = get_authenticated_admin(&app_state).await;
        let mut req_builder = test::TestRequest::with_uri("/review-publication")
            .method(Method::PUT)
            .header("Content-Type", "application/json")
            .header("Accept", "text/html")
            .set_payload(Bytes::from(
                serde_json::to_string(&ReviewGameData {
                    game_id: game_id.to_hex(),
                    review_value,
                })
                .unwrap(),
            ));

        if authenticated {
            req_builder = req_builder.cookie(get_auth_cookie(
                "mentalo-admin-auth",
                app_state
                    .encryption
                    .decrypt(&admin_user.auth_token.unwrap())
                    .to_owned(),
            ));
        }

        req_builder
    }

    #[tokio::test]
    async fn test_review_game_positive() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(review_game_publication),
        )
        .await;

        let (game_id, game_name) = insert_test_game(
            &app_state,
            Game::test_game(&app_state).unreviewed(&app_state),
        )
        .await
        .unwrap();

        let req = get_request(true, &app_state, &game_id, true)
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let should_be_reviewed_meta = app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": game_name}, None)
            .await
            .unwrap()
            .unwrap()
            .metadata
            .unwrap();

        assert!(should_be_reviewed_meta.reviewed_publication && !should_be_reviewed_meta.moderated);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_review_game_negative() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(review_game_publication),
        )
        .await;

        let (game_id, game_name) = insert_test_game(
            &app_state,
            Game::test_game(&app_state).unreviewed(&app_state),
        )
        .await
        .unwrap();

        let req = get_request(false, &app_state, &game_id, true)
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::OK);

        let should_be_reviewed_meta = app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": game_name}, None)
            .await
            .unwrap()
            .unwrap()
            .metadata
            .unwrap();

        assert!(should_be_reviewed_meta.reviewed_publication && should_be_reviewed_meta.moderated);

        let del_count = delete_test_game(&app_state, &game_id).await.unwrap();
        assert_eq!(del_count, 1);
    }

    #[tokio::test]
    async fn test_review_game_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(review_game_publication),
        )
        .await;

        let req = get_request(false, &app_state, &ObjectId::new(), false)
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
    }
}
