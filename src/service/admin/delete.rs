use crate::middleware::AuthenticatedAdminMiddleware;
use crate::model::Game;
use crate::AppState;
use actix_web::{
    delete,
    web::{Data, Path},
    HttpRequest, HttpResponse, Responder,
};
use wither::{
    bson::{doc, oid::ObjectId},
    prelude::Model,
};

/// Finds a game by id and deleted it permanently
#[delete("/delete/{game_id}")]
pub async fn delete_game(
    app_state: Data<AppState>,
    req: HttpRequest,
    middleware: Data<AuthenticatedAdminMiddleware<'_>>,
    game_id: Path<String>,
) -> impl Responder {
    if middleware.exec(&app_state, &req, None).await.is_err() {
        return HttpResponse::Unauthorized().finish();
    }

    let game_id = ObjectId::with_string(&game_id.into_inner())
        .expect("Failed to convert game_id to ObjectId. String may be malformed");

    match Game::find_one_and_delete(&app_state.db, doc! {"_id": &game_id}, None).await {
        Ok(result) => match result {
            Some(deleted) => HttpResponse::Accepted()
                .body(format!("Game {} was successfully deleted", deleted.name)),
            None => HttpResponse::NotFound()
                .body(format!("Game with id {} was not found", game_id.to_hex())),
        },
        Err(e) => HttpResponse::InternalServerError().body(format!("Error deleting game {:?}", e)),
    }
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_delete {
    use super::*;
    use crate::{
        middleware::get_auth_cookie,
        model::{AdminAuthCredentials, Administrator},
    };
    use actix_web::{
        http::{Method, StatusCode},
        test, App,
    };
    use wither::bson::Bson;

    async fn insert_test_game(app_state: &AppState) -> Result<(ObjectId, String), String> {
        let test_game = Game::test_game(app_state);
        let game_name = test_game.name.to_owned();
        match app_state
            .db
            .collection_with_type::<Game>("games")
            .insert_one(test_game, None)
            .await
        {
            Ok(inserted) => match inserted.inserted_id {
                Bson::ObjectId(id) => Ok((id, game_name)),
                _ => Err(String::from("Failed to parse inserted_id")),
            },
            Err(e) => Err(format!("{:?}", e)),
        }
    }

    async fn get_authenticated_admin(app_state: &AppState) -> Administrator {
        Administrator::authenticated(
            app_state,
            AdminAuthCredentials {
                username: app_state.env.mentalo_default_admin_username.to_owned(),
                password: app_state.env.mentalo_default_admin_password.to_owned(),
            },
        )
        .await
        .unwrap()
    }

    async fn get_request(
        app_state: &AppState,
        authenticated: bool,
        game_id: &ObjectId,
    ) -> test::TestRequest {
        let uri = format!("/delete/{}", game_id.to_hex());

        let mut req = test::TestRequest::with_uri(&uri).method(Method::DELETE);

        if authenticated {
            let admin_user = get_authenticated_admin(&app_state).await;

            req = req.cookie(get_auth_cookie(
                "mentalo-admin-auth",
                app_state
                    .encryption
                    .decrypt(&admin_user.auth_token.unwrap())
                    .to_owned(),
            ));
        }

        req
    }

    #[tokio::test]
    /// Publishes a game from a serialized test game instance and asserts with deserialization response contains the inserted_id
    /// Asserts that the game has been inserted by fetching it by name in database and asserts ids are matching
    async fn test_delete_game() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(delete_game),
        )
        .await;

        let (game_id, game_name) = insert_test_game(&app_state).await.unwrap();
        let req = get_request(&app_state, true, &game_id).await.to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::ACCEPTED);

        let should_be_not_found = app_state
            .db
            .collection_with_type::<Game>("games")
            .find_one(doc! {"name": game_name}, None)
            .await
            .unwrap();

        assert!(should_be_not_found.is_none());
    }

    #[tokio::test]
    /// Publishes a game from a serialized test game instance and asserts with deserialization response contains the inserted_id
    /// Asserts that the game has been inserted by fetching it by name in database and asserts ids are matching
    async fn test_delete_game_no_auth_should_be_unauthorized() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(delete_game),
        )
        .await;

        let (game_id, _) = insert_test_game(&app_state).await.unwrap();

        let req = get_request(&app_state, false, &game_id).await.to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::UNAUTHORIZED);

        Game::find_one_and_delete(&app_state.db, doc! {"_id": &game_id}, None)
            .await
            .expect("Error deleting test game");
    }

    #[tokio::test]
    /// Generated a whatever ObjectId and calls the delete_game service. Asserts that the response is 404.
    /// It sends the request with the proper auth cookie because Unauthorized is thrown before NotFound.
    async fn test_delete_game_wrong_id_should_be_not_found() {
        dotenv::dotenv().ok();

        let app_state = AppState::for_test().await;

        let mut app = test::init_service(
            App::new()
                .app_data(Data::new(app_state.clone()))
                .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                    "mentalo-admin-auth",
                )))
                .service(delete_game),
        )
        .await;

        let req = get_request(&app_state, true, &ObjectId::new())
            .await
            .to_request();

        let res = test::call_service(&mut app, req).await;

        assert_eq!(res.status(), StatusCode::NOT_FOUND);
    }
}
