use crate::translations::get_translated_email_body;
use crate::AppState;
use actix_web::web::Data;
use lettre::transport::smtp::authentication::Credentials;
use lettre::{
    message::{Mailbox, Message, MultiPart},
    Address, SmtpTransport, Transport,
};

/// A helper to send emails
/// Takes a to address, a subject and a multipart body as arguments and returns an HttpResponse
/// The multipart body is simply represented as a fixed sized array of 2 strings.
pub fn send_email(
    app_state: &Data<AppState>,
    to: &String,
    subject: String,
    multipart_plain_html: [String; 2],
) -> Result<String, String> {
    let context = std::env::var("CONTEXT").unwrap_or(String::from("default"));
    if context.eq("testing") {
        return Ok(String::from("testing"));
    }

    let from = Mailbox::new(
        None,
        app_state
            .env
            .mentalo_email_sender
            .parse::<Address>()
            .unwrap(),
    );

    let to = Mailbox::new(None, to.parse::<Address>().unwrap());

    let creds = Credentials::new(
        app_state.env.mailjet_api_key.to_owned(),
        app_state.env.mailjet_api_pwd.to_owned(),
    );

    let email = Message::builder()
        .from(from)
        .to(to)
        .subject(subject)
        .multipart(MultiPart::alternative_plain_html(
            multipart_plain_html[0].to_owned(),
            multipart_plain_html[1].to_owned(),
        ))
        .unwrap();

    let mailer = SmtpTransport::relay(&app_state.env.mailjet_smtp_server_addr)
        .unwrap()
        .credentials(creds)
        .build();

    match mailer.send(&email) {
        Ok(_) => Ok(String::from("Email was send with success.")),
        Err(e) => Err(format!("Could not send email: {:?}", e)),
    }
}

/// Returns the email multipart body for a publishing game confirmation.
pub fn publish_game_confirmation_email_multipart_body(
    app_state: &AppState,
    game_id: &String,
    game_name: &String,
    confirm_publication_token: &String,
    locale: &str,
) -> [String; 2] {
    let route = "game/confirm-publication";
    let confirmation_link = format!(
        "http://{}/{}/{}/{}",
        app_state.env.server_host, route, game_id, confirm_publication_token
    );

    let [mut raw, mut html] = get_translated_email_body("confirm_publication", locale);

    raw = raw
        .replace("{game_name}", game_name)
        .replace("{link}", &confirmation_link);

    html = html
        .replace("{game_name}", game_name)
        .replace("{link}", &confirmation_link);

    [raw, html]
}

/// Returns the multipart body for an updating game confirmation
pub fn update_game_confirmation_email_multipart_body(
    app_state: &AppState,
    game_id: &String,
    game_name: &String,
    confirm_update_token: &String,
    locale: &str,
) -> [String; 2] {
    let route = "game/confirm-update";
    let confirmation_link = format!(
        "http://{}/{}/{}/{}",
        app_state.env.server_host, route, game_id, confirm_update_token
    );

    let [mut raw, mut html] = get_translated_email_body("confirm_update", locale);

    raw = raw
        .replace("{game_name}", game_name)
        .replace("{link}", &confirmation_link);

    html = html
        .replace("{game_name}", game_name)
        .replace("{link}", &confirmation_link);

    [raw, html]
}

/// Returns the multipart body for an updating game confirmation
pub fn request_game_deletion_confirmation_email_multipart_body(
    app_state: &AppState,
    game_id: &String,
    game_name: &String,
    confirmation_token: &String,
    locale: &str,
) -> [String; 2] {
    let route = "game/confirm-deletion";

    let confirmation_link = format!(
        "http://{}/{}/{}/{}",
        app_state.env.server_host, route, game_id, confirmation_token
    );

    let [mut raw, mut html] = get_translated_email_body("confirm_deletion_request", locale);

    raw = raw
        .replace("{game_name}", game_name)
        .replace("{link}", &confirmation_link);

    html = html
        .replace("{game_name}", game_name)
        .replace("{link}", &confirmation_link);

    [raw, html]
}

/// Returns the multipart body for the administrator notification when a game is published and confirmed by its author.
pub fn game_publication_admin_notif_multipart_body(
    game_name: &String,
    game_id: &String,
    user_email: &String,
) -> [String; 2] {
    let raw = format!(
        "
    A new game was published on Mentalo and is waiting for review:\n
    - Game name: {}\n
    - Game id: {}\n
    - Author email: {}
    ",
        game_name, game_id, user_email
    );

    let html = format!(
        "
    <strong>A new game was published on Mentalo and is waiting for review:</strong><br />
    <ul>
        <li>Game name: {}</li>
        <li>Game id: {}</li>
        <li>Author email: {}</li>
    </ul>
    ",
        game_name, game_id, user_email
    );
    [raw, html]
}

/// Returns the email body for the administrator notification of a game update.
pub fn game_update_admin_notif_multipart_body(
    game_name: &String,
    game_id: &String,
    user_email: &String,
) -> [String; 2] {
    let raw = format!(
        "
    A game has been updated:\n
    - Game name: {}\n
    - Game id: {}\n
    - Author email: {}
    ",
        game_name, game_id, user_email
    );

    let html = format!(
        "
    <strong>A game has been updated:</strong><br />
    <ul>
        <li>Game name: {}</li>
        <li>Game id: {}</li>
        <li>Author email: {}</li>
    </ul>
    ",
        game_name, game_id, user_email
    );
    [raw, html]
}

/// Returns the email body for the administrator notification of a game deletion request
pub fn game_deletion_request_admin_notif_multipart_body(
    game_name: &String,
    game_id: &String,
) -> [String; 2] {
    let raw = format!(
        "
    A game has been requested for deletion:\n
    - Game name: {}\n
    - Game id: {}
    ",
        game_name, game_id
    );

    let html = format!(
        "
    <strong>A game has been requested for deletion:</strong><br />
    <ul>
        <li>Game name: {}</li>
        <li>Game id: {}</li>
    </ul>
    ",
        game_name, game_id
    );
    [raw, html]
}

/// Returns the email body for the notification sent to the author of a game when the review has been made.
pub fn game_review_author_notification_multipart_body(
    game_name: &String,
    positive_review: bool,
    locale: &str,
) -> [String; 2] {
    let trad_review_key = match positive_review {
        true => "positive_review",
        false => "negative_review",
    };

    let [mut raw, mut html] = get_translated_email_body(trad_review_key, locale);
    raw = raw.replace("{game_name}", game_name);
    html = html.replace("{game_name}", game_name);
    [raw, html]
}

#[cfg(test)]
mod test_no_email_for_test {
    use super::*;

    #[tokio::test]
    async fn should_pass_in_testing_context() {
        dotenv::dotenv().ok();
        let app_state = AppState::for_test().await;

        let res = send_email(
            &Data::new(app_state),
            &String::from("test"),
            String::from("test"),
            [String::new(), String::new()],
        );

        assert_eq!(res.unwrap(), "testing");
    }
}
