use crate::AppState;
use actix_files::NamedFile;
use actix_web::web::Data;

pub async fn favicon() -> actix_web::Result<NamedFile> {
    let standard_dir = std::path::PathBuf::from(
        std::env::var("STATIC_RESOURCES_DIR").expect("STATIC_RESOURCES_DIR is not defined"),
    )
    .join("static/standard");

    Ok(NamedFile::open(standard_dir.join("favicon.ico"))?)
}

pub async fn robots(app_state: Data<AppState>) -> actix_web::Result<NamedFile> {
    let standard_dir = std::path::PathBuf::from(
        std::env::var("STATIC_RESOURCES_DIR").expect("STATIC_RESOURCES_DIR is not defined"),
    )
    .join("static/standard");

    match app_state.env.release_mode.as_str() {
        "prod" => Ok(NamedFile::open(standard_dir.join("robots.txt"))?),
        _ => Ok(NamedFile::open(standard_dir.join("test.robots.txt"))?),
    }
}

pub async fn sitemap() -> actix_web::Result<NamedFile> {
    let standard_dir = std::path::PathBuf::from(
        std::env::var("STATIC_RESOURCES_DIR").expect("STATIC_RESOURCES_DIR is not defined"),
    )
    .join("static/standard");

    Ok(NamedFile::open(standard_dir.join("sitemap.xml"))?)
}
