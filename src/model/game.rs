use serde::{Deserialize, Serialize};
use wither::{
    bson::{doc, oid::ObjectId, DateTime},
    prelude::Model,
};
pub mod game_fields;
#[cfg(test)]
use crate::AppState;
#[cfg(test)]
use chrono::Utc;
use game_fields::*;

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The metadata of a pending update. Holds the date of the update,
/// and a token that will be used by the user to confirm the update with the emailed confirmation link.
pub struct PendingGameUpdateMetaData {
    pub created_on: DateTime,
    pub confirm_update_token: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// This is the type for the Game::metadata::pending_update field and represents the fields of a game that can be updated.
///
/// When a new version of a game is published, the data is stored in that structure.
/// When the user confirms the update, the data stored in here will
/// replace the corresponding fields of the game, and the pending_update field will be set to None
pub struct PendingGameUpdateData {
    pub scenes: Vec<Scene>,
    pub game_ui_options: UiOptions,
    pub starting_scene_index: isize,
    pub publishing_overview: GamePublishingOverview,
    pub metadata: PendingGameUpdateMetaData,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The field that is used to allw tha author of a game to request the deletion of that game
pub struct RequestedDeletion {
    pub confirmation_token: String,
    pub confirmed: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The type for the Game::metadata field.
/// It holds the data related to the game but not read by the game engine.
pub struct GameMetadata {
    pub author_email: String,
    pub confirmed_publication: bool,
    pub reviewed_publication: bool,
    pub created_on: DateTime,
    pub last_update_on: DateTime,
    pub moderated: bool,
    pub confirm_publication_token: String,
    pub version: isize,
    pub pending_update: Option<PendingGameUpdateData>,
    pub requested_deletion: Option<RequestedDeletion>,
    pub prefered_language: String,
}

impl GameMetadata {
    #[cfg(test)]
    /// Returns a default test GameMetadata instance
    pub fn test_metadata(app_state: &AppState) -> Self {
        GameMetadata {
            author_email: app_state
                .encryption
                .encrypt(&String::from("test@email.com")),
            confirmed_publication: true,
            reviewed_publication: true,
            created_on: DateTime(Utc::now()),
            last_update_on: DateTime(Utc::now()),
            moderated: false,
            confirm_publication_token: app_state
                .encryption
                .encrypt(&app_state.encryption.random_ascii_lc_string(24)),
            version: 1,
            pending_update: None,
            requested_deletion: None,
            prefered_language: String::from("en"),
        }
    }

    #[cfg(test)]
    /// Returns a test metadata instance with a pending update.
    pub fn with_test_pending_update(&self, app_state: &AppState) -> Self {
        let mut updated_metadata = self.clone();
        updated_metadata.pending_update = Some(PendingGameUpdateData {
            scenes: Vec::<Scene>::new(),
            game_ui_options: UiOptions::test_options(),
            starting_scene_index: 2,
            publishing_overview: GamePublishingOverview {
                author_name: String::from("test author name"),
                author_website: Some(String::from("http://updated-website.com")),
                description: String::from("updated description"),
                banner_image: String::from("********************"),
            },
            metadata: PendingGameUpdateMetaData {
                created_on: DateTime(Utc::now()),
                confirm_update_token: app_state
                    .encryption
                    .encrypt(&(app_state.encryption.random_ascii_lc_string(24))),
            },
        });

        updated_metadata
    }

    #[cfg(test)]
    /// Returns a test Gamemetadata instance mocking a game which has not been confirmed by the author yet.
    pub fn unconfirmed(&self) -> Self {
        let mut updated_metadata = self.clone();
        updated_metadata.confirmed_publication = false;
        updated_metadata.reviewed_publication = false;
        updated_metadata
    }

    #[cfg(test)]
    /// Returns a test Gamemetadata instance mocking a game which has been confirmed by author
    /// but not reviewed by an admin yet.
    pub fn unreviewed(&self) -> Self {
        let mut updated_metadata = self.clone();
        updated_metadata.confirmed_publication = true;
        updated_metadata.reviewed_publication = false;
        updated_metadata
    }

    #[cfg(test)]
    /// Returns a test Gamemetadata instance with the `moderated` flag set to true.
    pub fn moderated(&self) -> Self {
        let mut updated_metadata = self.clone();
        updated_metadata.confirmed_publication = true;
        updated_metadata.reviewed_publication = true;
        updated_metadata.moderated = true;
        updated_metadata
    }

    #[cfg(test)]
    /// Returns a test Gamemetadata instance with a requested deletion
    pub fn with_requested_deletion(&self, app_state: &AppState, confirmed: bool) -> Self {
        let mut updated_metadata = self.clone();
        updated_metadata.confirmed_publication = true;
        updated_metadata.reviewed_publication = true;
        updated_metadata.requested_deletion = Some(RequestedDeletion {
            confirmation_token: app_state
                .encryption
                .encrypt(&(app_state.encryption.random_ascii_lc_string(24))),
            confirmed,
        });
        updated_metadata
    }
}

#[derive(Debug, Deserialize, Serialize, Clone, Model)]
#[model(index(keys = r#"doc!{"name": 1}"#, options = r#"doc!{"unique": true}"#))]
/// The model for a game document.
/// The game json document produced on the application side must be deserialized into this type before being stored into database.
pub struct Game {
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
    pub id: Option<ObjectId>,
    pub name: String,
    pub scenes: Vec<Scene>,
    pub game_ui_options: UiOptions,
    pub starting_scene_index: isize,
    pub publishing_overview: GamePublishingOverview,
    pub metadata: Option<GameMetadata>,
    pub license: Option<String>,
    pub resources_index: ResourcesIndex,
}

impl Game {
    /// If a Game instance has a pending update in the game metadat, the data of the new version will replace
    /// the values of the corresponding fields of the Game instance.
    pub fn apply_pending_update(&mut self) -> Result<(), &'static str> {
        let mut game_meta = match self.metadata {
            Some(ref mut meta) => meta,
            None => return Err("Game metadata is none."),
        };

        let pending_update = match game_meta.pending_update {
            Some(ref pu) => pu,
            None => return Err("No pending update was found for that game"),
        };

        self.scenes = pending_update.scenes.clone();
        self.game_ui_options = pending_update.game_ui_options.clone();
        self.starting_scene_index = pending_update.starting_scene_index;
        self.publishing_overview = pending_update.publishing_overview.clone();

        game_meta.last_update_on = pending_update.metadata.created_on;
        game_meta.version += 1;
        game_meta.pending_update = None;
        Ok(())
    }

    /// Returns the email used to manage the game.
    pub fn get_author_email(&self) -> Result<String, &'static str> {
        match self.metadata {
            Some(ref md) => Ok(md.author_email.to_string()),
            None => Err("Metadata is none"),
        }
    }

    #[cfg(test)]
    /// Generates a ready made Game instance with mockup data for testing.
    pub fn test_game(app_state: &AppState) -> Self {
        Game {
            id: None,
            name: String::from("testgame"),
            scenes: Vec::<Scene>::new(),
            game_ui_options: UiOptions::test_options(),
            starting_scene_index: 2,
            publishing_overview: GamePublishingOverview::test_overview(),
            metadata: Some(GameMetadata::test_metadata(app_state)),
            license: Some(String::from("https://creativecommons.org/licenses/by/4.0/")),
            resources_index: ResourcesIndex::new(),
        }
    }

    #[cfg(test)]
    /// Generates a test Game instance, with a mocked pending update.
    pub fn with_test_pending_update(&self, app_state: &AppState) -> Self {
        let mut updated = self.clone();
        updated.metadata =
            Some(GameMetadata::test_metadata(app_state).with_test_pending_update(app_state));
        updated
    }

    #[cfg(test)]
    /// Generates a test game instance that has not been confirmed by the author
    pub fn unconfirmed(&self, app_state: &AppState) -> Self {
        let mut updated = self.clone();
        updated.metadata = Some(GameMetadata::test_metadata(app_state).unconfirmed());
        updated
    }

    #[cfg(test)]
    /// Generates a test game instance that has been confirmed by the author but not reviewed by moderation yet.
    pub fn unreviewed(&self, app_state: &AppState) -> Self {
        let mut updated = self.clone();
        updated.metadata = Some(GameMetadata::test_metadata(app_state).unreviewed());
        updated
    }

    #[cfg(test)]
    /// Generates a test game instance that has been moderated.
    pub fn moderated(&self, app_state: &AppState) -> Self {
        let mut updated = self.clone();
        updated.metadata = Some(GameMetadata::test_metadata(app_state).moderated());
        updated
    }

    #[cfg(test)]
    /// Generates a test game instance with a request for deletion
    pub fn with_requested_deletion(&self, app_state: &AppState, confirmed: bool) -> Self {
        let mut updated = self.clone();
        updated.metadata = Some(
            GameMetadata::test_metadata(app_state).with_requested_deletion(&app_state, confirmed),
        );
        updated
    }
}
