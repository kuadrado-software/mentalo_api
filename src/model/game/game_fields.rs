//! The model::Game structure is very nested so this is a module to define all the sub types of a Game document.

use serde::{Deserialize, Serialize};
use wither::bson::doc;

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The x y position of a GameObject
pub struct Position {
    x: isize,
    y: isize,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// A GameObject that can be hold by a Scene.
/// It has a name, an image name, and an x y postion.
pub struct GameObject {
    name: String,
    image: String,
    position: Position,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The animation of a scene.
/// It has a name, a single base64 string image as th src attribute that should hold all the frames (if > 1) next to each other in a row.
/// A number of frame
/// The animation speed
/// A boolean flag used if the animation should only be played once.
pub struct Animation {
    name: String,
    speed: i16,
    play_once: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// A structure representing the soundtrack of a scene.
/// Has a name, a base64 string encoding the audio data (src)
/// A loop flag indicating wheter or not the track should play in a loop.
pub struct SoundTrack {
    name: String,
    _loop: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// A item of the required object list that can be defined in a choice.
pub struct ChoiceUseObjectsItem {
    /// true if the object must be removed from inventory after having been used for the choice.
    consume_object: bool,
    /// the name of the object
    name: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The field of a choice that indicates if objects are required to make the choice.
pub struct ChoiceUseObjects {
    /// true if objects are required
    value: bool,
    /// The list of the required objects
    items: Vec<ChoiceUseObjectsItem>,
    /// The message to display if the player doesn't have the required objects
    missing_object_message: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// A choice represents one of the possible actions that can be made in a scene.
pub struct Choice {
    /// The text displayed on the button
    text: String,
    /// The index of the scene the player will be directed to after making that choice
    destination_scene_index: isize,
    /// Defines if the choice requires to use some objects.
    use_objects: ChoiceUseObjects,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The options defining the behavior of the game at the end of a cinematic.
pub struct EndCinematicOptions {
    /// The index of the scene the player will be directed to once the cinematic reaches end.
    destination_scene_index: isize,
    /// true if this is the end of the program.
    quit: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// A Scene is the mainn component of a game.
/// The players moves from scene to scene by clicking the choices available in each scene.
pub struct Scene {
    /// The name of the scene
    name: String,
    /// The type of the scene. Can be "playable" or "cinematic"
    _type: String,
    /// The animation played for that scene
    animation: Animation,
    /// The sound track played in that scene
    sound_track: SoundTrack,
    /// The choices that can me made in that scene
    choices: Vec<Choice>,
    /// A message that will be displayed in a text box when the player arrives into the scene.
    /// Will not be displayed if empty string
    text_box: String,
    /// If the scene is a cinematic, the duration in seconds before applying what's defined in the EndCinematicOptions
    cinematic_duration: isize,
    /// The objects located onto the scene image.
    game_objects: Vec<GameObject>,
    /// If the scene is a cinematic, this will be used at the end of the cinnematic.
    end_cinematic_options: EndCinematicOptions,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The size and shape of the container holding the image of each scene.
pub struct AnimCanvasDims {
    /// A with in pixel
    width: i32,
    /// A ratio used to calculate the height of the canvas. Can be "4:3" "16:9" "14:10" etc...
    ratio: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The styling options for the general game interface
pub struct GeneralUiOptions {
    background_color: String,
    animation_canvas_dimensions: AnimCanvasDims,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The styling option for the text boxes of each scene
pub struct TextBoxesOptions {
    background_color: String,
    font_size: i16,
    font_style: String,
    font_weight: String,
    font_family: String,
    font_color: String,
    text_align: String,
    padding: i16,
    margin: i16,
    border_width: i16,
    rounded_corners_radius: i16,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The styling options for the panel in which are displayed the choices of a scene
pub struct ChoicesPanelOptions {
    background_color: String,
    font_size: i16,
    font_family: String,
    font_color: String,
    font_style: String,
    text_align: String,
    font_weight: String,
    container_padding: f32,
    choice_padding: f32,
    active_choice_background_color: String,
    active_choice_border_width: i16,
    active_choice_rounded_corners_radius: i16,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// The styling option for the inventory panel of the game board
pub struct InventoryOptions {
    background_color: String,
    columns: i16,
    rows: i16,
    gap: i16,
    padding: i16,
    slot_rounded_corner_radius: i16,
    slot_border_width: i16,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// A set of styling options for the different parts of the game interface
pub struct UiOptions {
    general: GeneralUiOptions,
    text_boxes: TextBoxesOptions,
    choices_panel: ChoicesPanelOptions,
    inventory: InventoryOptions,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
/// This is metadata used to display some overview information of a game.
/// This data is defined by the author of the game at the moment of the pubilshing.
pub struct GamePublishingOverview {
    pub author_name: String,
    pub author_website: Option<String>,
    pub description: String,
    pub banner_image: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ImageResource {
    pub name: String,
    pub src: String,
    pub frame_nb: Option<isize>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct SoundResource {
    pub name: String,
    pub src: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ResourcesIndex {
    pub images: Vec<ImageResource>,
    pub sounds: Vec<SoundResource>,
}

impl ResourcesIndex {
    #[cfg(test)]
    pub fn new() -> Self {
        ResourcesIndex {
            images: Vec::new(),
            sounds: Vec::new(),
        }
    }
}

impl UiOptions {
    #[cfg(test)]
    /// Generates testing mockup data
    pub fn test_options() -> Self {
        UiOptions {
            general: GeneralUiOptions {
                background_color: String::from("#000"),
                animation_canvas_dimensions: AnimCanvasDims {
                    width: 800,
                    ratio: String::from("4:3"),
                },
            },
            text_boxes: TextBoxesOptions {
                background_color: String::from("#000"),
                font_size: 16,
                font_style: String::from("normal"),
                font_weight: String::from("bold"),
                font_family: String::from("monospace"),
                font_color: String::from("white"),
                text_align: String::from("center"),
                padding: 20,
                margin: 20,
                border_width: 4,
                rounded_corners_radius: 4,
            },
            choices_panel: ChoicesPanelOptions {
                background_color: String::from("#000"),
                font_size: 16,
                font_family: String::from("monospace"),
                font_color: String::from("white"),
                font_style: String::from("normal"),
                text_align: String::from("center"),
                font_weight: String::from("bold"),
                container_padding: 5.0,
                choice_padding: 5.0,
                active_choice_background_color: String::from("#fff5"),
                active_choice_border_width: 15,
                active_choice_rounded_corners_radius: 4,
            },
            inventory: InventoryOptions {
                background_color: String::from("#000"),
                columns: 2,
                rows: 3,
                gap: 5,
                padding: 10,
                slot_rounded_corner_radius: 10,
                slot_border_width: 2,
            },
        }
    }
}

impl GamePublishingOverview {
    #[cfg(test)]
    /// Testing mockup data
    pub fn test_overview() -> Self {
        GamePublishingOverview {
            author_name: String::from("testauthor"),
            author_website: None,
            description: String::from("test description"),
            banner_image: String::from("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAAAAAAeW/F+AAAABGdBTUEAALGPC"),
        }
    }
}
