use super::Game;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
/// The data type corresponding to the expected format when publishing a game.
pub struct PublishGameData {
    pub game: Game,
    pub user_email: String,
}

#[derive(Debug, Deserialize, Serialize)]
/// The data type corresponding to the expected format when sending a game review (admin)
pub struct ReviewGameData {
    pub game_id: String,
    pub review_value: bool,
}

/// This value is used for the accreditation of the Creative Common license on each game.
/// This field is the one that should be modified in case we want to change the license for the games.
pub const PUBLICATION_LICENSE_TEXT: &'static str = "https://creativecommons.org/licenses/by/4.0/";
