//! # REST API server for the Mentalo application
mod app_state;
mod crypto;
mod env;
mod init_admin;
mod middleware;
mod model;
mod mw;
mod service;
mod standard_static_files;
mod tls;
mod translations;
mod view;
mod view_resource;
use actix_files::Files;
use actix_web::{
    middleware::{normalize::TrailingSlash, Logger, NormalizePath},
    web::{get, resource, scope, to, Data, JsonConfig},
    App, HttpResponse, HttpServer,
};
use app_state::AppState;
use env::get_log_level;
use env_logger::Env;
use middleware::AuthenticatedAdminMiddleware;
use model::publishing::PUBLICATION_LICENSE_TEXT;
use mw::https_redirect::RedirectHTTPS;
use service::{admin::*, admin_auth::admin_auth, games::*};
use standard_static_files::{favicon, robots, sitemap};
use std::env::var as env_var;
use tls::get_tls_config;
use view::get_view;
use view_resource::{ViewResourceDescriptor, ViewResourceManager};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or(get_log_level())).init();

    let app_state = AppState::with_default_admin_user().await;

    let server_port = env_var("SERVER_PORT").expect("SERVER_PORT is not defined.");
    let server_port_tls = env_var("SERVER_PORT_TLS").expect("SERVER_PORT_TLS is not defined.");
    let static_dir = std::path::PathBuf::from(
        env_var("STATIC_RESOURCES_DIR").expect("STATIC_RESOURCES_DIR is not defined"),
    )
    .join("static");

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            // Redirect all requests to https
            .wrap(RedirectHTTPS::with_replacements(&[(
                format!(":{}", server_port),
                format!(":{}", server_port_tls),
            )]))
            .wrap(actix_web::middleware::Compress::default())
            .app_data(Data::new(app_state.clone()))
            .app_data(Data::new(AuthenticatedAdminMiddleware::new(
                "mentalo-admin-auth",
            )))
            // Allow json payload to have size until ~32MB
            .app_data(JsonConfig::default().limit(1 << 25u8))
            .app_data(Data::new(ViewResourceManager::with_views(vec![
                ViewResourceDescriptor {
                    path_str: "admin-panel",
                    index_file_name: "index.html",
                    resource_name: "admin-panel",
                    apply_auth_middleware: true,
                },
                ViewResourceDescriptor {
                    path_str: "admin-login",
                    index_file_name: "index.html",
                    resource_name: "admin-login",
                    apply_auth_middleware: false,
                },
                ViewResourceDescriptor {
                    path_str: "404",
                    index_file_name: "404.html",
                    resource_name: "404",
                    apply_auth_middleware: false,
                },
                ViewResourceDescriptor {
                    path_str: "unauthorized",
                    index_file_name: "unauthorized.html",
                    resource_name: "unauthorized",
                    apply_auth_middleware: false,
                },
                ViewResourceDescriptor {
                    path_str: "publishing-policy",
                    index_file_name: "index.html",
                    resource_name: "publishing-policy",
                    apply_auth_middleware: false,
                },
            ])))
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // REST API /////////////////////////////////////////////////////////////////////////////////////////////////
            .wrap(NormalizePath::new(TrailingSlash::Trim))
            .service(
                scope("/game")
                    .service(games_list)
                    .service(search_game)
                    .service(game_by_id)
                    .service(game_overview_by_id)
                    .service(game_by_name)
                    .service(publish_game)
                    .service(confirm_game_publication)
                    .service(update_game)
                    .service(confirm_game_update)
                    .service(request_delete_game)
                    .service(confirm_game_deletion_request)
                    .service(send_confirmation_email),
            )
            .service(
                resource("/publishing/license-attribution-link")
                    .route(get().to(|| HttpResponse::Ok().body(PUBLICATION_LICENSE_TEXT))),
            )
            .service(
                scope("/admin")
                    .service(get_pending_reviews)
                    .service(review_game_publication)
                    .service(delete_game)
                    .service(get_moderated)
                    .service(get_requested_deletions)
                    .service(get_decrypted_string),
            )
            .service(admin_auth)
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // STANDARD FILES ///////////////////////////////////////////////////////////////////////////////////////////
            .service(resource("/favicon.ico").route(get().to(favicon)))
            .service(resource("/robots.txt").route(get().to(robots)))
            .service(resource("/sitemap.xml").route(get().to(sitemap)))
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // VIEWS & STATIC CONTENT ///////////////////////////////////////////////////////////////////////////////////
            .service(
                scope("/v")
                    .service(Files::new(
                        "/admin-panel/assets",
                        static_dir.join("admin-panel/assets"),
                    ))
                    .service(Files::new(
                        "/admin-login/assets",
                        static_dir.join("admin-login/assets"),
                    ))
                    .service(
                        Files::new("/publishing-policy", static_dir.join("publishing-policy"))
                            .index_file("index.html"),
                    )
                    // get_view will match any url to we put it at last
                    .service(get_view),
            )
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // MENTALO APP //////////////////////////////////////////////////////////////////////////////////////////////
            .service(Files::new("/fr", static_dir.join("mentalo-app")).index_file("index-fr.html"))
            .service(Files::new("/es", static_dir.join("mentalo-app")).index_file("index-es.html"))
            .service(Files::new("/en", static_dir.join("mentalo-app")).index_file("index.html"))
            .service(Files::new("/", static_dir.join("mentalo-app")).index_file("index.html"))
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // empty 404 ////////////////////////////////////////////////////////////////////////////////////////////////
            .default_service(to(|| {
                HttpResponse::NotFound().body("<h1>404 - Page not found</h1>")
            }))
    })
    .bind(format!("0.0.0.0:{}", env_var("SERVER_PORT").unwrap()))?
    .bind_rustls(
        format!("0.0.0.0:{}", env_var("SERVER_PORT_TLS").unwrap()),
        get_tls_config(),
    )?
    .run()
    .await
}
