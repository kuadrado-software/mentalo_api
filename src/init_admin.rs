use crate::model::Administrator;
use crate::AppState;
use wither::{bson::doc, prelude::Model};

/// Creates the default administrator if it doesn't already exists and returns a Result.
pub async fn create_default_admin_if_none(app_state: &AppState) -> Result<(), String> {
    let admin_username = app_state.env.mentalo_default_admin_username.to_owned();
    let admin_password = app_state.env.mentalo_default_admin_password.to_owned();

    let admin = Administrator::from_values(app_state, admin_username, admin_password);

    let admin_doc = doc! {
        "username": &admin.username,
        "password_hash": &admin.password_hash
    };

    match Administrator::find_one(&app_state.db, admin_doc, None).await {
        Ok(found_user) => match found_user {
            Some(_) => Ok(()),
            None => {
                println!("Mentalo admin will be created");
                match app_state
                    .db
                    .collection_with_type::<Administrator>("administrators")
                    .insert_one(admin, None)
                    .await
                {
                    Ok(_) => Ok(()),
                    Err(e) => Err(format!("Error creating administrator: {:?}", e)),
                }
            }
        },
        Err(e) => Err(format!("Error creating administrator: {:?}", e)),
    }
}
