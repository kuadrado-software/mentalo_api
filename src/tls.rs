use rustls::{
    internal::pemfile::{certs, pkcs8_private_keys},
    NoClientAuth, ServerConfig,
};
use std::{env::var as env_var, fs::File, io::BufReader};

/// Parse the TLS certificates presents in ./certs/live/${SERVER_HOST} and returns a rustls::ServerConfig that can be passed to
/// the actix_web::HttpServer instance:
/// ```
/// HttpServer::new(move || {
///     App::new()   
/// })
/// .bind_rustls(
///     "0.0.0.0:8080",
///     get_tls_config(),
/// )?
/// .run()
/// .await
/// ```
pub fn get_tls_config() -> ServerConfig {
    let host = env_var("SERVER_HOST").expect("SERVER_HOST is not defined.");

    let mut config = ServerConfig::new(NoClientAuth::new());
    let certs_dir = std::path::PathBuf::from(
        env_var("STATIC_RESOURCES_DIR").expect("STATIC_RESOURCES_DIR is not defined"),
    )
    .join("certs")
    .join("live")
    .join(&host);

    let cert_file = File::open(certs_dir.join("fullchain.pem")).unwrap();
    let key_file = File::open(certs_dir.join("privkey.pem")).unwrap();

    let cert_file = &mut BufReader::new(cert_file);
    let key_file = &mut BufReader::new(key_file);

    let cert_chain = certs(cert_file).unwrap();
    let mut keys = pkcs8_private_keys(key_file).unwrap();

    config.set_single_cert(cert_chain, keys.remove(0)).unwrap();

    config
}
