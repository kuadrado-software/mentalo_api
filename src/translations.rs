pub static SUPPORTED_LOCALES: [&'static str; 3] = ["en", "fr", "es"];

pub static TRANSLATIONS: [[&'static str; 3]; 17] = [
    [
        "Mentalo - Please confirm your publication",
        "Mentalo - Veuillez confirmer votre publication",
        "Mentalo - Por favor, confirma tu publicación",
    ],
    [
        "Mentalo - Please confirm your game update",
        "Mentalo - Veuillez confirmer la mise à jour de votre jeu",
        "Mentalo - Por favor, confirma la actualización de tu juego",
    ],
    [
        "Game is managed by a different email address",
        "Le jeu est géré par une adresse email différente",
        "El juego es gestionado por una dirección de correo electrónico diferente",
    ],
    [
        "Requested game was not found",
        "Le jeu demandé n'a pas été trouvé",
        "No se ha encontrado el juego solicitado",
    ],
    [
        "No pending update was found for that game",
        "Aucune mise à jour en attente n'a été trouvée pour ce jeu",
        "No se ha encontrado ninguna actualización pendiente para ese juego",
    ],
    [
        "Mentalo - Your game is online!",
        "Mentalo - Votre jeu est en ligne !",
        "Mentalo - ¡Tu juego está en línea!",
    ],
    [
        "Requested game was not found",
        "Le jeu demandé est introuvable",
        "No se ha encontrado el juego solicitado",
    ],
    [
        "Mentalo - Please confirm your game update",
        "Mentalo - Veuillez confirmer la mise à jour de votre jeu",
        "Mentalo - Por favor, confirma la actualización de tu juego",
    ],
    [
        "Game update has been successfully received",
        "La mise à jour du jeu a été reçue avec succès",
        "La actualización del juego ha sido recibida con éxito",
    ],
    [
        "The publication of the following game was successfully confirmed",
        "La publication du jeu suivant a été confirmée avec succès",
        "Se ha confirmado con éxito la publicación del siguiente juego",
    ],
    [
        "It will be accessible online as soon as it will be validated by the Mentalo moderation.",
        "Il sera accessible en ligne dès qu'il aura été validé par la modération de Mentalo.",
        "Será accesible en línea tan pronto como sea validado por la moderación de Mentalo.",
    ],
    [
        "The following game was successfully updated",
        "Le jeu suivant a été mis à jour avec succès",
        "El siguiente juego fue actualizado con éxito",
    ],
    [
        "Mentalo- Your game publication has been rejected by moderation",
        "Mentalo - Votre jeu a été rejeté par la modération",
        "Mentalo - La publicación de tu juego ha sido rechazada por la moderación",
    ],
    [
        "Mentalo - Confirm the request for deletion of your game",
        "Mentalo - Confirmez la demande de suppression de votre jeu",
        "Mentalo - Confirma la solicitud de eliminación de tu juego"
    ],
    [
        "The request for deletion has been sucessfully confirmed for the following game",
        "La demande de suppression pour le jeu suivant a été confirmée avec succès",
        "La solicitud de eliminación del siguiente juego ha sido confirmada con éxito",
    ],
    [
        "No request for deletion was found for that game",
        "Aucune demande de suppression n'a été trouvée pour ce jeu",
        "No se ha encontrado ninguna solicitud de eliminación para ese juego",
    ],

    // TEST TRANSLATION ///////////////////////////////////////////
    [
        "Testing translations",
        "Tests de traductions",
        "Probando traducciones",
    ],
];


static CONFIRM_PUBLICATION_EMAIL_TRANSATION: [[&'static str; 2];3] = [
    ["
    Congratulations!\n
    Please confirm the publication of your new game \"{game_name}\" by pasting the following link in your browser:\n\n{link}\n
    Your game will be accessible by everyone as soon as it will be reviewed by moderation.\n
    Thank you!
    ",
    "
    <h2>Congratulations!</h2>
    <p>Please confirm the publication of your new game <b>{game_name}</b> by clicking the following link:<br/><br />
    <a href='{link}' target='_blank'>{link}</a><br/><br />
    Your game will be accessible by everyone as soon as it will be reviewed by moderation.<br /><br />
    Thank you!</p>
    "],
    ["
    Félicitations !\n
    Merci de confirmer la publication de votre nouveau jeu \"{game_name}\" en collant le lien ci-dessous dans votre navigateur:\n\n{link}\n
    Votre jeu sera accessible à tous dès qu'il aura été validé par la modération.\n
    Merci !
    ",
    "
    <h2>Félicitations !</h2>
    <p>Merci de confirmer la publication de votre nouveau jeu <b>{game_name}</b> en cliquant sur le lien suivant :<br/><br />
    <a href='{link}' target='_blank'>{link}</a><br/><br />
    Votre jeu sera accessible à tous dès qu'il aura été validé par la modération.<br /><br />
    Merci !</p>
    "],
    ["
    ¡Enhorabuena!\n
    Por favor, confirma la publicación de tu nuevo juego \"{game_name}\" pegando el siguiente enlace en tu navegador:\n\n{link}\n
    Tu juego será accesible para todos tan pronto como sea revisado por la moderación.\n
    ¡Gracias!
    ",
    "
    <h2>¡Enhorabuena!</h2>
    <p>Por favor, confirma la publicación de tu nuevo juego <b>{game_name}</b> pegando el siguiente enlace en tu navegador:<br/><br />
    <a href='{link}' target='_blank'>{link}</a><br/><br />
    Tu juego será accesible para todos tan pronto como sea revisado por la moderación.<br /><br />
    ¡Gracias!</p>
    "],
    
];

static CONFIRM_UPDATE_EMAIL_TRANSATION: [[&'static str; 2];3] = [
    ["
    You sent a new version of your game {game_name}!\n
    Please confirm the publication of the new version by clicking on the link below:\n{link}\n\n
    Thank you!
    ",
    "
    <h2>You sent a new version of your game {game_name}!</h2>
    <p>Please confirm the publication of the new version by clicking on the link below:<br/><br />
    <a href='{link}' target='_blank'>{link}</a><br/><br />
    Thank you!</p>
    "],
    ["
    Vous avez envoyé une nouvelle version de votre jeu {game_name} !\n
    Veuillez confirmer la publication de la nouvelle version en cliquant sur le lien ci-dessous:\n{link}\n\n
    Merci !
    ",
    "
    <h2>Vous avez envoyé une nouvelle version de votre jeu {game_name} !</h2>
    <p>Veuillez confirmer la publication de la nouvelle version en cliquant sur le lien ci-dessous:<br/><br />
    <a href='{link}' target='_blank'>{link}</a><br/><br />
    Merci !</p>
    "],
    ["
    ¡Has enviado una nueva versión de tu juego {game_name}!\n
    Por favor, confirma la publicación de la nueva versión haciendo clic en el siguiente enlace:\n{link}\n\n
    ¡Gracias!
    ",
    "
    <h2>¡Has enviado una nueva versión de tu juego {game_name}!</h2>
    <p>Por favor, confirma la publicación de la nueva versión haciendo clic en el siguiente enlace:<br/><br />
    <a href='{link}' target='_blank'>{link}</a><br/><br />
    ¡Gracias!</p>
    "],
];

static POSITIVE_REVIEW_NOTIFICATION_EMAIL_BODY :[[&'static str; 2];3]= [
    ["
    Congratulations!\n
    The publication of your game {game_name} has been validated by Mentalo moderation and is now available online.\n\n
    Thank you!
    ",
    "
    <h2>Congratulations!</h2>
    <p>The publication of your game <b>{game_name}</b> has been validated by Mentalo moderation and is now available online.
    <br /><br />Thank you!</p>
    "
    ],
    ["
    Félicitations !\n
    La publication de votre jeu {game_name} a été validée par la modération de Mentalo et est à présent accessible en ligne.\n\n
    Merci !
    ",
    "
    <h2>Félicitations!</h2>
    <p>La publication de votre jeu <b>{game_name}</b> a été validée par la modération de Mentalo et est à présent accessible en ligne.
    <br /><br />Merci !</p>
    "
    ],
    ["
    ¡Enhorabuena!\n
    La publicación de tu juego {game_name} ha sido validada por la moderación de Mentalo y ya está disponible en línea.\n\n
    ¡Gracias!
    ",
    "
    <h2>Congratulations!</h2>
    <p>La publicación de tu juego {game_name} ha sido validada por la moderación de Mentalo y ya está disponible en línea.
    <br /><br />¡Gracias!</p>
    "
    ]
];

static NEGATIVE_REVIEW_NOTIFICATION_EMAIL_BODY: [[&'static str; 2];3] = [
    ["
    Sorry...\n
    The publication of your game {game_name} has been rejected by Mentalo moderation.\n
    Please read our publication policy and try again after having modified your game.\n\n
    Thank you.
    ",
    "
    <h4>Sorry...</h4>
    <p>The publication of your game {game_name} has been rejected by Mentalo moderation.<br />
    Please read our publication policy and try again after having modified your game.
    <br /><br />Thank you.</p>
    "
    ],
    ["
    Désolé...\n
    La publication de votre jeu {game_name} a été rejetée par la modération de Mentalo.\n
    Veuillez lire notre charte de publication et réessayez après avoir modifié votre jeu.\n\n
    Merci.
    ",
    "
    <h4>Désolé...</h4>
    <p>La publication de votre jeu {game_name} a été rejetée par la modération de Mentalo.<br />
    Veuillez lire notre charte de publication et réessayez après avoir modifié votre jeu.
    <br /><br />Merci.</p>
    "
    ],
    ["
    Lo sentimos...\n
    The publication of your game {game_name} has been rejected by Mentalo moderation.\n
    Por favor, lee nuestra política de publicación y vuelve a intentarlo después de haber modificado tu juego.\n\n
    Gracias.
    ",
    "
    <h4>Lo sentimos...</h4>
    <p>La publicación de tu juego {game_name} ha sido rechazada por la moderación de Mentalo.<br />
    Por favor, lee nuestra política de publicación y vuelve a intentarlo después de haber modificado tu juego.
    <br /><br />Gracias.</p>
    "
    ]
];

static CONFIRM_REQUEST_GAM_DELETION_EMAIL_BODY :[[&'static str; 2];3]= [
    ["
    You requested the deletion of your game {game_name}.\n
    If you want to confirm this action, click on the link below.\n\n{link}\n\n 
    The deletions must be done manually by an administrator so it will be effective as soon the person in charge treats your demand.\n
    After that your game will have been completely removed from database.\n
    Thank you.
    ",
    "
    <h3>You requested the deletion of your game {game_name}.</h3>
    <p>If you want to confirm this action, click on the link below.
    <br /><br /><a href='{link}' target='_blank'>{link}</a><br /><br />
    The deletions must be done manually by an administrator so it will be effective as soon the person in charge treats your demand.
    <br />
    After that your game will have been completely removed from database.<br />
    Thank you, see you soon !
    </p>
    "
    ],
    ["
    Vous avez demandé la suppression de votre jeu {game_name}.\n
    Si vous souhaitez confirmer cette action, merci de cliquer sur le lien ci-dessous.\n\n{link}\n\n
    Les suppressions devant être faîtes manuellement par un administrateur, celle-ci sera effective dès que la personne responsable traitera votre demande.\n
    Après ça votre jeu sera complètement supprimé de la base de données.\n
    Merci, à bientôt !
    ",
    "
    <h3>Vous avez demandé la suppression de votre jeu {game_name}.</h3>
    <p>Si vous souhaitez confirmer cette action, merci de cliquer sur le lien ci-dessous.
    <br /><br /><a href='{link}' target='_blank'>{link}</a><br /><br />
    Les suppressions devant être faîtes manuellement par un administrateur, celle-ci sera effective dès que la personne responsable traitera votre demande.
    <br />
    Après ça votre jeu sera complètement supprimé de la base de données.<br />
    Merci, à bientôt !
    </p>
    "
    ],
    ["
    Has solicitado la eliminación de tu juego {game_name}.\n
    Si deseas confirmar esta acción, haz clic en el siguiente enlace.\n\n{link}\n\n 
    Las eliminaciones deben ser realizadas manualmente por un administrador por lo que ésta será efectiva en cuanto el responsable atienda tu demanda.\n
    Después de eso tu juego habrá sido completamente eliminado de la base de datos.\n
    Gracias, hasta pronto.
    ",
    "
    <h3>Has solicitado la eliminación de tu juego {game_name}.</h3>
    <p>Si deseas confirmar esta acción, haz clic en el siguiente enlace.
    <br /><br /><a href='{link}' target='_blank'>{link}</a><br /><br />
    Las eliminaciones deben ser realizadas manualmente por un administrador por lo que ésta será efectiva en cuanto el responsable atienda tu demanda.
    <br />
    Después de eso tu juego habrá sido completamente eliminado de la base de datos.<br />
    Gracias, hasta pronto.
    </p>
    "
    ],
];

static TEST_EMAIL: [[&'static str; 2]; 3] = [
    ["Some raw email text", "<p>Some html text<p/>"],
    ["Du texte brut", "<p>Du texte html<p/>"],
    ["Texto bruto", "<p>Texto html<p/>"],
];

pub fn get_translated(key_str: &str, locale: &str) -> String {
    let locale_index = match SUPPORTED_LOCALES.iter().position(|&sl| sl.eq(locale)) {
        Some(i) => i,
        None => 0,
    };

    match TRANSLATIONS.iter().find(|&&trad| trad[0].eq(key_str)) {
        Some(trad) => trad[locale_index].to_string(),
        None => key_str.to_string(),
    }
}

pub fn get_translated_email_body(email_type: &str, locale: &str) -> [String; 2] {
    let locale_index = match SUPPORTED_LOCALES.iter().position(|&l| l.eq(locale)) {
        Some(i) => i,
        None => 0,
    };

    let trad = match email_type {
        "confirm_publication" => CONFIRM_PUBLICATION_EMAIL_TRANSATION[locale_index],
        "confirm_update" => CONFIRM_UPDATE_EMAIL_TRANSATION[locale_index],
        "positive_review" => POSITIVE_REVIEW_NOTIFICATION_EMAIL_BODY[locale_index],
        "negative_review" => NEGATIVE_REVIEW_NOTIFICATION_EMAIL_BODY[locale_index],
        "confirm_deletion_request" => CONFIRM_REQUEST_GAM_DELETION_EMAIL_BODY[locale_index],
        "test" => TEST_EMAIL[locale_index],
        _ => ["", ""],
    };

    [String::from(trad[0]), String::from(trad[1])]
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *  _______   ______    ______   _______   *@@
 * |__   __@ |  ____@  /  ____@ |__   __@  *@@
 *    |  @   |  @__    \_ @_       |  @    *@@
 *    |  @   |   __@     \  @_     |  @    *@@
 *    |  @   |  @___   ____\  @    |  @    *@@
 *    |__@   |______@  \______@    |__@    *@@
 *                                         *@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@@
 *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*@*/

#[cfg(test)]
mod test_translations {
    use super::*;

    #[test]
    fn should_get_translated_fr() {
        let key = "Testing translations";
        let locale = "fr";
        let trad = get_translated(key, locale);
        assert_eq!(trad, "Tests de traductions");
    }

    #[test]
    fn should_get_translated_es() {
        let key = "Testing translations";
        let locale = "es";
        let trad = get_translated(key, locale);
        assert_eq!(trad, "Probando traducciones");
    }

    #[test]
    fn should_return_key_if_wrong_key() {
        let key = "Unexisting translation key";
        let locale = "fr";
        let trad = get_translated(key, locale);
        assert_eq!(trad, key);
    }

    #[test]
    fn should_return_key_if_wrong_locale() {
        let key = "Testing translations";
        let locale = "wronglocale";
        let trad = get_translated(key, locale);
        assert_eq!(trad, key);
    }

    #[test]
    fn should_return_key_and_english_equals() {
        let key = "Testing translations";
        let locale = "en";
        let trad = get_translated(key, locale);
        assert_eq!(trad, key);
    }

    #[test]
    fn should_get_translated_email_fr() {
        let trad = get_translated_email_body("test", "fr");
        assert_eq!(trad, ["Du texte brut", "<p>Du texte html<p/>"]);
    }

    #[test]
    fn should_get_translated_email_es() {
        let trad = get_translated_email_body("test", "es");
        assert_eq!(trad, ["Texto bruto", "<p>Texto html<p/>"],);
    }

    #[test]
    fn should_get_translated_email_en() {
        let trad = get_translated_email_body("test", "en");
        assert_eq!(trad, ["Some raw email text", "<p>Some html text<p/>"]);
    }

    #[test]
    fn should_get_translated_email_en_if_wrong_locale() {
        let trad = get_translated_email_body("test", "zaejozaeijz");
        assert_eq!(trad, ["Some raw email text", "<p>Some html text<p/>"]);
    }

    #[test]
    fn should_get_translated_email_emapty_strings_if_wrong_key() {
        let trad = get_translated_email_body("azeoiuzaoeiu", "fr");
        assert_eq!(trad, ["", ""]);
    }
}
