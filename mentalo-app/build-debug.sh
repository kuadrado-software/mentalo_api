#!/bin/bash
read_var() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<<"$VAR"
    echo ${VAR[1]}
}

SERVER_URL=$(read_var SERVER_PROTOCOL .env)://$(read_var SERVER_HOST .env):$(read_var SERVER_PORT_OUT .env)
cwd=$(pwd)

rm -rf /tmp/mentalo-app-dist
mkdir /tmp/mentalo-app-dist
cd /tmp/mentalo-app-dist

wget https://gitlab.com/kuadrado-software/mentalo-app/-/raw/master/dist/mentalo-app-dist.tar.gz

tar -xvzf mentalo-app-dist.tar.gz
npm install
SERVER_URL=$SERVER_URL npm run build

cd $cwd
rm -rf ./static/mentalo-app
mkdir -p ./static/mentalo-app
cp -r /tmp/mentalo-app-dist/public/* ./static/mentalo-app/
rm -rf /tmp/mentalo-app-dist
