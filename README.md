# REST API server for the Mentalo application

## Installation for Development

### Prerequisites

-   Linux based distribution
-   Git
-   Docker
-   Docker-compose
-   Nodejs / npm
-   mkcert (localhost) | Certbot (real server)

---

### TLS CERTIFICATES

-   ### Localhost

    I recommand to use mkcert to generate self-signed certificates https://github.com/FiloSottile/mkcert.

    ```sh
    $ sudo mkdir -p /etc/letsencrypt/live/localhost
    $ sudo chmod 777 -R /etc/letsencryt/live/localhost
    $ mkcert -key-file privkey.pem -cert-file fullchain.pem localhost
    ```

-   ### Real server

    Install snapd and certbot at system level. https://certbot.eff.org/lets-encrypt/debianbuster-other

    Then run

    ```sh
    $ sudo certbot certonly --standalone
    # Fill the prompt accordingly to what's provided in your env variables for the server host.
    ```

The certs and symlinks located in `/etc/letsencrypt/*` will be all mounted in the container at `/**/mentalo_api/certs/`

---

## Build

```sh
$ git clone https://gitlab.com/kuadrado-software/mentalo_api.git
$ cd mentalo_api

$ touch ./.env
# Fill your .env file with the required variables described below

# Build the admin frontend
$ cd admin-frontend
$ npm install
$ npm run build
# or if you want to build the bundle non-minified
$ npm run build-debug

$ cd ..

# Download the latest Mentalo-app distribution and build it in the static resources.
$ ./mentalo-app/build.sh
# Or to build the unminified bundle for debug
$ ./mentalo-app/build-debug.sh

# build the image and run the container
$ docker-compose up --build

# Or if for a local dev environment
$ docker-compose -f ./dev.docker-compose.yml up --build
# This will mount the source code in the container so you can quickly restart the rust container and recompile it without losing cache
# - Make changes in Rust code ...
# - run $ docker-compose restart rust_api
```

Once everything is built and compiled the server should be listenning to requests at `${SERVER_HOST}:${SERVER_PORT_OUT}` with SERVER_HOST and SERVER_PORT_OUT defined in your .env file.

---

## Expected environment variables

_Env vars may be defined in a .env file at the root of the project_

-   `RELEASE_MODE`: Can be either "debug", "test" or "prod". It's used to define the log level. Not required. Default is "prod"
-   `DATABASE_NAME`: The name of the database you will log into to use the services
-   `DB_ROOT_USERNAME`: The name of the root user of the Mongodb instance
-   `DB_ROOT_PASSWORD`: The password of the root user of the Mongodb instance
-   `DB_USERNAME`: Name of the non-root user of the Mongodb instance (created in init-mongodb.js)
-   `DB_USER_PASSWORD`: Password of the non-root user of the Mongodb instance
-   `DB_PORT`: The port exposed by the Mongodb database (usually 27017)
-   `SERVER_PORT`: The port bound the Actix server
-   `SERVER_PORT_TLS`: The tls port bound the Actix server
-   `SERVER_HOST`: IP address or domain name hosting the API.
-   `SERVER_PROTOCOL`: http or https regarding the protocol used to connect the server
-   `MENTALO_EMAIL_SENDER`: The email address used to send automatic email notifications to users
-   `MENTALO_EMAIL_RECEIVER`: The email address used to receive administrator notifications (ex: new publications waiting for validation)
-   `MENTALO_DEFAULT_ADMIN_USERNAME`: The name of the default app user with admin role (the user which perform validations etc.)
-   `MENTALO_DEFAULT_ADMIN_PASSWORD`: The password of the default app admin user
-   `MENTALO_ADMIN_EMAIL`: The email address of the default admin user (authentication & notifications)
-   `MAILJET_API_KEY`: The api key of the smtp relay used for emailing
-   `MAILJET_API_PWD`: The password of the smtp service
-   `MAILJET_SMTP_SERVER_ADDR`: The url of the smtp service
-   `CRYPT_KEY`: A string that will be used by encryption to encrypt emails and passwords. A randomly generated string can be used.
-   `STATIC_RESOURCES_DIR`: A linux directory that will be used to mount static resources like certs and html files

---

## Testing

Tests can be run with

```sh
$ cargo test -- --test-threads=1
```

The limit of 1 thread is because test games documents are created and deleted from database and it cannot happen in parallel. (duplicate key error because test games are all the same name)

In order to run the tests, the mongo container must be running.
The actix::App config for testing will connect the DB using host `localhost`.

The .env file is also required to run the tests.

If some test fails, it can happen that the testgame document is not removed correctly from database so you may have to do it manually sometimes:

```sh
$ docker exec -it ${DATABASE_NAME} bash
root@somecontainerid:/$ mongo -u ${DB_USERNAME} --authenticationDatabase ${DATABASE_NAME} -p ${DB_USER_PASSWORD}
...
MongoDB shell version v5.0.3
connecting to: mongodb://127.0.0.1:27017/?...
...
================
> use ${DATABASE_NAME}
switched to db ${DATABASE_NAME}
> db.games.deleteOne({ name: "testgame" })
{ "acknowledged" : true, "deletedCount" : 1 }
> exit

```

---

## Database

The database is a Mongodb instance which is initialized with 2 collections:

-   **games** (The publications of the users)
    -   Model: `crate::model::Game`
    -   indexes: (fields that must be unique and are used by db for indexation)
        -   `name` (Games are index by name with an index of type `"text"` in order to allow a text research on game names)
-   **administrators** (Authenticated users who will have access to the admin frontend and can perform admin actions such as moderation, reviews, etc)
    -   Model: `crate::model::Administrator`
    -   Indexes
        -   `username`
        -   `auth_token`

### Dump and restore
1) **Dump**

    Login to a shell into the database container
    ```sh
    $ docker exec -it mentalodb bash
    ```
    Use the mongodump convenience script
    ```sh
    $ /mongo-scripts/dump.sh
    ```
    Dump folder will be located at `mentalo_api/mongodumps/`

2) **Send the dump to another server**

    To send the backup to another machine use the `scp` command.

    Set up ssh authorized_keys on destination machine before exectuing following commands.

    ```sh
    $ cd mentalo_api
    $ scp -r ./mongodumps/ USERNAME@HOST:/home/USERNAME/DESTINATION_DIR
    ```

    Where USERNAME, HOST and DESTINATION_DIR depends on your environment and choices.

3) **Restore a backup**
    Assuming you previously sent a dump with scp from another machine. The dump should be located at `/home/USERNAME/DESTINATION_DIR`.

    Copy the dump into the `mentalo_api/mongodumps` directory
    ```sh
    $ cd mentalo_api
    $ cp -R ~/DESTINATION_DIR/dump ./mongodumps/
    ```

    Connect to a shell into the database container
    ```sh
    $ docker exec -it mentalodb bash
    ```

    Use the mongorestore conveniencescript
    ```sh
    $ /mongo-scripts/restore.sh
    ```